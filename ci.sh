#!/usr/bin/env bash

###
## This script replicates the same steps as ci, except for cargo audit
###

# Turn on the guard rails
set -exuo pipefail

## TODO use subshell magic to make a nice interface here

# Lint the formatting
nix build .#lints.format.replicator -L
# Audit it
cargo audit
# Run clippy
nix build .#lints.clippy.replicator -L
# Build it
nix build .#replicator -L
# Test it
nix build .#tests.replicator -L
# Document it
nix build .#docs.replicator.doc -L
