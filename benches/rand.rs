use criterion::{criterion_group, criterion_main, Criterion, Throughput};

use replicator::rand::rand;

// Write at least `total` bytes, in chunks of size `chunk_size`
fn generate(total: usize, chunk_size: usize) -> Vec<u8> {
    let mut output: Vec<u8> = Vec::with_capacity(total + chunk_size);
    let mut total_written = 0;
    let mut buffer = vec![0_u8; chunk_size];
    while total_written < total {
        rand(&mut buffer).expect("Failed to generate random bytes");
        output.extend(&buffer);
        total_written += chunk_size;
    }
    output
}

fn bench_generate(c: &mut Criterion) {
    let mut group = c.benchmark_group("Chunked throughput");
    // Generate 8192 chunks of size 4096, this will total 64MiB
    group.throughput(Throughput::Bytes(8_192 * 4_096));
    group.bench_function("Total: 128MiB Chunk: 4kiB", |b| {
        b.iter(|| generate(8_192 * 4_096, 4_096));
    });
}

criterion_group!(benches, bench_generate);
criterion_main!(benches);
