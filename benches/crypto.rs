use criterion::{criterion_group, criterion_main, Criterion, Throughput};

use replicator::crypto::{Cipher, Hmac, Key, MasterKey, Nonce};

fn encrypt(c: &mut Criterion) {
    // Make and leak key
    let key = Key::random().unwrap();
    let key: &'static Key = Box::leak(Box::new(key));
    // Make group
    let mut group = c.benchmark_group("XChaCha20");
    // 1kiB test
    group.throughput(Throughput::Bytes(1_024));
    group.bench_function("1kiB", |b| {
        b.iter(|| {
            let mut data = [0_u8; 1_024];
            let mut cipher = Cipher::new(key, Nonce::zero());
            cipher.apply_keystream(&mut data).unwrap();
        });
    });
    group.throughput(Throughput::Bytes(4_096));
    group.bench_function("4kiB", |b| {
        b.iter(|| {
            let mut data = [0_u8; 4_096];
            let mut cipher = Cipher::new(key, Nonce::zero());
            cipher.apply_keystream(&mut data).unwrap();
        });
    });
    group.throughput(Throughput::Bytes(8_192));
    group.bench_function("8kiB", |b| {
        b.iter(|| {
            let mut data = [0_u8; 8_192];
            let mut cipher = Cipher::new(key, Nonce::zero());
            cipher.apply_keystream(&mut data).unwrap();
        });
    });
    group.throughput(Throughput::Bytes(16_384));
    group.bench_function("16kiB", |b| {
        b.iter(|| {
            let mut data = [0_u8; 16_384];
            let mut cipher = Cipher::new(key, Nonce::zero());
            cipher.apply_keystream(&mut data).unwrap();
        });
    });
}

fn hmac(c: &mut Criterion) {
    // Make and leak key
    let key = Key::random().unwrap();
    let key: &'static Key = Box::leak(Box::new(key));
    // Make and leak our hmac
    let hmac: &'static Hmac<'static> = Box::leak(Box::new(Hmac::new(key)));
    // Make group
    let mut group = c.benchmark_group("Blake3");
    // 1kiB test
    let bytes_1k: &'static [u8] = vec![0_u8; 1_024].leak();
    group.throughput(Throughput::Bytes(1_024));
    group.bench_function("1kiB", |b| {
        b.iter(|| {
            hmac.tag(bytes_1k);
        });
    });
    // 4kiB test
    let bytes_4k: &'static [u8] = vec![0_u8; 4_096].leak();
    group.throughput(Throughput::Bytes(4_096));
    group.bench_function("4kiB", |b| {
        b.iter(|| {
            hmac.tag(bytes_4k);
        });
    });
    // 8kiB test
    let bytes_8k: &'static [u8] = vec![0_u8; 8_192].leak();
    group.throughput(Throughput::Bytes(8_192));
    group.bench_function("8kiB", |b| {
        b.iter(|| {
            hmac.tag(bytes_8k);
        });
    });
    // 16kiB test
    let bytes_16k: &'static [u8] = vec![0_u8; 16_384].leak();
    group.throughput(Throughput::Bytes(16_384));
    group.bench_function("16kiB", |b| {
        b.iter(|| {
            hmac.tag(bytes_16k);
        });
    });
}

fn key_creation(c: &mut Criterion) {
    let mut group = c.benchmark_group("Key Creation");
    group.throughput(Throughput::Elements(1));
    // Key generation
    group.bench_function("Key", |b| {
        b.iter(|| Key::random().unwrap());
    });
    // Master key generation
    group.bench_function("MasterKey", |b| {
        b.iter(|| MasterKey::random().unwrap());
    });
    // Derivation
    let master_key: &'static MasterKey = Box::leak(Box::new(MasterKey::random().unwrap()));
    group.bench_function("Derivation", |b| {
        b.iter(|| master_key.derive().unwrap());
    });
}

fn encrypt_mac(c: &mut Criterion) {
    // Make and leak key
    let key = Key::random().unwrap();
    let key: &'static Key = Box::leak(Box::new(key));
    // Make and leak our hmac
    let hmac: &'static Hmac<'static> = Box::leak(Box::new(Hmac::new(key)));
    let mut group = c.benchmark_group("Encrypt Then MAC");
    // 1kiB test
    group.throughput(Throughput::Bytes(1_024));
    group.bench_function("1kiB", |b| {
        b.iter(|| {
            let mut bytes = [0_u8; 1024];
            let mut cipher = Cipher::new(key, Nonce::zero());
            cipher.apply_keystream(&mut bytes).unwrap();
            hmac.tag(&bytes)
        });
    });
    // 4kiB test
    group.throughput(Throughput::Bytes(4_096));
    group.bench_function("4kiB", |b| {
        b.iter(|| {
            let mut bytes = [0_u8; 4_096];
            let mut cipher = Cipher::new(key, Nonce::zero());
            cipher.apply_keystream(&mut bytes).unwrap();
            hmac.tag(&bytes)
        });
    });
    // 8kiB test
    group.throughput(Throughput::Bytes(8_192));
    group.bench_function("8kiB", |b| {
        b.iter(|| {
            let mut bytes = [0_u8; 8_192];
            let mut cipher = Cipher::new(key, Nonce::zero());
            cipher.apply_keystream(&mut bytes).unwrap();
            hmac.tag(&bytes)
        });
    });
    // 16kiB test
    group.throughput(Throughput::Bytes(16_384));
    group.bench_function("16kiB", |b| {
        b.iter(|| {
            let mut bytes = [0_u8; 16_384];
            let mut cipher = Cipher::new(key, Nonce::zero());
            cipher.apply_keystream(&mut bytes).unwrap();
            hmac.tag(&bytes)
        });
    });
}

criterion_group!(benches, encrypt, hmac, key_creation, encrypt_mac);
criterion_main!(benches);
