#!/usr/bin/env bash
# Clean the workspace first
cargo llvm-cov clean
cargo llvm-cov nextest --open --ignore-filename-regex "bin/replicate.rs"
