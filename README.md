# Replicator

Replicator is a highly work-in-progess library containing primatives for implementing and
interacting with secure, encrypted, authenticated archives

## Crypto Suite

Replicator is very intentionally not generic over the cryptography primatives used, see [the
author's blog](https://mccarty.io/chacha20-blake3/) for more context. The currently used set of
primatives is as follows

| Usage                  | Primative                                                           |
|------------------------|---------------------------------------------------------------------|
| Password Hashing       | [Balloon Hashing](https://crypto.stanford.edu/balloon/) with Blake3 |
| Encryption             | XChaCha20                                                           |
| Authentication/Hashing | Blake3                                                              |
| KDF                    | Blake3                                                              |

All data and metadata in a Replicator archive is authenticated with Blake3-MAC.

# <span style="color:red">**Saftey Warning**</span>

The author of this library has done his best to provide a curated choice of cryptographic primavites
that is difficult to misuse, and to provide a misuse resistant api on top of the primavites that
should, ideally, be suitable for general use.

This, however, is no guarntee of saftey. Not only is the author but a faillable, error prone human,
but cryptography is _complicated_. Complicated interactions can occur between cryptographic
primitives when they are combined together, with potentally catastrophic unexpected conseqences. Any
analysis of the security of your cryptosystem must be hollistic, and you must consider how your
application interacts with this library's use of the underlying cryptographic primatives.

I have tried to compile a set of cryptographic primatives that avoid most of the common pitfalls,
however, that is no guarntee of your saftey, I can not guard against every potential interaction. If
you do not feel qualified to make such an analysis, it is probably wise to reconsider use of this
library. 

That said, I'm not here to tell you what to do, go nuts, just whatever happens, _you have been
warned_, and the author cannot be held responsible for the results of your own hubris.
