//! This Module contains tests for the MVP
use hexdump::hexdump;
use replicator::{file::EncryptedFile, rand::rand};
use snafu::{ResultExt, Whatever};
use std::{
    fmt::Debug,
    io::{Cursor, Read, Seek, SeekFrom, Write},
};
use tempfile::tempfile;

pub const PASSWORD: &[u8] = b"A totally secure password";

/// High Level Targets
mod target1_high_level {
    use super::*;
    /// Make sure we can initalize, close, and reopen an encrypted file
    mod initalize {
        use super::*;
        /// Generic function to test the initialize -> close -> reopen workflow with stored keys
        /// with the given (NEW!!!) file-like object
        fn stored_key<T: Read + Write + Seek + Debug>(file: T) -> Result<T, Whatever> {
            // Create a derived key encrypted file
            let mut enc_file = EncryptedFile::create_stored(file, PASSWORD, 16 * 1024)
                .whatever_context("Failed to initialize the file")?;
            // Flush it and close it up
            enc_file.flush().whatever_context("Failed to flush")?;
            let mut file = enc_file.into_inner();
            // Seek back to beginning of our "file"
            file.seek(SeekFrom::Start(0))
                .whatever_context("Failed to seek")?;
            // Try to open the file back up
            let enc_file = EncryptedFile::open_stored(file, PASSWORD)
                .whatever_context("Failed to reopen file")?;
            // Finish test
            Ok(enc_file.into_inner())
        }

        /// Test workflow with a cursor
        #[test]
        fn stored_key_cursor() -> Result<(), Whatever> {
            // Get our "file"
            let file: Cursor<Vec<u8>> = Cursor::new(Vec::new());
            // Run the test
            let output = stored_key(file)?;
            // Dump the content of the "file"
            let vec = output.into_inner();
            hexdump(&vec);
            println!("Length: {}", vec.len());
            Ok(())
        }

        /// Test workflow with a file
        #[test]
        fn stored_key_file() -> Result<(), Whatever> {
            // Get our file
            let file = tempfile().whatever_context("Failed to get tempfile")?;
            // Run the test
            let _ = stored_key(file)?;
            Ok(())
        }
    }
    /// Make sure we can append bytes to a file and read them back out, without closing the file
    mod append {
        use snafu::FromString;

        use super::*;
        /// Generic function to test the initialize -> append -> read workflow with stored keys,
        /// with a short payload, and not using the `Write` imp
        fn stored_key_short_no_write<T: Read + Write + Seek + Debug>(
            file: T,
        ) -> Result<T, Whatever> {
            // Create a stored key encrypted file
            let mut enc_file = EncryptedFile::create_stored(file, PASSWORD, 16 * 1024)
                .whatever_context("Failed to initalize the file")?;

            // Get some random bytes to write
            let mut bytes = [0_u8; 256];
            rand(&mut bytes).whatever_context("Failed to get random bytes")?;

            // Append the bytes to the file
            enc_file
                .append_bytes(bytes.to_vec())
                .whatever_context("Failed to append bytes")?;

            // Seek back to the start of the file and read them backout
            enc_file
                .seek(SeekFrom::Start(0))
                .whatever_context("Failed to seek to start")?;
            let mut buffer = [0_u8; 256];
            enc_file
                .read_exact(&mut buffer[..])
                .whatever_context("Failed to read back bytes")?;

            println!("{}", enc_file.debug());
            // Check to see if we got the right bytes back
            if bytes == buffer {
                Ok(enc_file.into_inner())
            } else {
                println!("bytes:  {:?}", bytes);
                println!("buffer: {:?}", buffer);
                Err(Whatever::without_source("Bytes did not match".to_string()))
            }
        }
        /// Generic function to test the initialize -> append -> read workflow with stored keys,
        /// with a long payload, and not using the `Write` imp
        fn stored_key_long_no_write<T: Read + Write + Seek + Debug>(
            file: T,
        ) -> Result<T, Whatever> {
            // Create a stored key encrypted file
            let mut enc_file = EncryptedFile::create_stored(file, PASSWORD, 16 * 1024)
                .whatever_context("Failed to initalize the file")?;

            // Get some random bytes to write
            let mut bytes = [0_u8; 64];
            rand(&mut bytes).whatever_context("Failed to get random bytes")?;

            // Append the bytes to the file
            for chunk in bytes.chunks(16) {
                enc_file
                    .append_bytes(chunk.to_vec())
                    .whatever_context("Failed to append bytes")?;
            }

            // Seek back to the start of the file and read them back out
            enc_file
                .seek(SeekFrom::Start(0))
                .whatever_context("Failed to seek to start")?;
            let mut buffer = [0_u8; 64];
            enc_file
                .read_exact(&mut buffer[..])
                .whatever_context("Failed to read back bytes")?;

            println!("{}", enc_file.debug());

            // Check to see if we got the right bytes back
            if bytes == buffer {
                Ok(enc_file.into_inner())
            } else {
                println!("bytes:  {:?}", bytes);
                println!("buffer: {:?}", buffer);
                Err(Whatever::without_source("Bytes did not match".to_string()))
            }
        }

        /// Recursively displays an error message
        fn display_error(e: impl std::error::Error) {
            println!("Error: {}\nCaused By:", e);
            let mut source = e.source();
            while let Some(s) = source {
                println!("  - {}", s);
                source = s.source();
            }
        }

        /// Test short workflow with a cursor
        #[test]
        fn stored_key_short_cursor_no_write() {
            // Get our "file"
            let file: Cursor<Vec<u8>> = Cursor::new(Vec::new());
            // Run the test
            match stored_key_short_no_write(file) {
                Ok(output) => {
                    // Dump the content of the "file"
                    let vec = output.into_inner();
                    hexdump(&vec);
                    println!("Length: {}", vec.len());
                }
                Err(e) => {
                    // display the error message
                    display_error(e);
                    panic!("Test failed");
                }
            }
        }

        /// Test long workflow with a cursor and no write
        #[test]
        fn stored_key_long_cursor_no_write() {
            // Get our "file"
            let file: Cursor<Vec<u8>> = Cursor::new(Vec::new());
            // Run the test
            match stored_key_long_no_write(file) {
                Ok(output) => {
                    // Dump the content of the "file"
                    let vec = output.into_inner();
                    hexdump(&vec);
                    println!("Length: {}", vec.len());
                }
                Err(e) => {
                    // display the error message
                    display_error(e);
                    panic!("Test failed");
                }
            }
        }

        /// Test short workflow with a file
        #[test]
        fn stored_key_short_file_no_write() {
            // Get our "file"
            let file = tempfile().expect("Failed to get tempfile");
            // run the test
            if let Err(e) = stored_key_short_no_write(file) {
                // display the error message
                display_error(e);
                panic!("Test failed");
            }
        }

        /// Test long workflow with a file
        #[test]
        fn stored_key_long_file_no_write() {
            // Get our "file"
            let file = tempfile().expect("Failed to get tempfile");
            // run the test
            if let Err(e) = stored_key_long_no_write(file) {
                // display the error message
                display_error(e);
                panic!("Test failed");
            }
        }
    }
}
