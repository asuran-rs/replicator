//! Error types used by the crate

pub use crate::{
    crypto::{cipher::CipherError, hmac::HmacEncodeError, key::KeyError},
    file::{
        traits::encode::SliceError,
        types::{action::ActionError, header::HeaderError},
    },
};
