//! Transparent encrypted + authenticated abstraction over  a file

use snafu::{ensure, ResultExt, Snafu};
use std::{
    collections::{HashMap, HashSet},
    fmt::Debug,
    io::{self, Read, Seek, SeekFrom, Write},
};
use tracing::{debug, error, info, instrument, trace};

pub mod traits;
pub mod types;

use crate::{
    crypto::{Cipher, Hmac, HmacTag, Key, MasterKey, Nonce, PasswordEncryptedMasterKey},
    file::{
        traits::Encode,
        types::{Action, ActionType, Header, NonceHandler, BYTES_PAYLOAD_OFFSET},
    },
};

/// Error interacting with an [`EncryptedFile`]
#[derive(Debug, Snafu)]
pub enum EncryptedFileError {
    /// Error deriving key
    Derive {
        /// The underlying error
        source: crate::error::KeyError,
    },
    /// Error generating a master key
    MasterKeyGen {
        /// The underlying error
        source: crate::error::KeyError,
    },
    /// Error decrypting a master key
    MasterKeyDecrypt {
        /// The underlying error
        source: crate::error::KeyError,
    },
    /// Error encoding header
    HeaderEncode {
        /// The underlying error
        source: crate::error::HeaderError,
    },
    /// Error decoding header
    HeaderDecode {
        /// The underlying error
        source: crate::error::HeaderError,
    },
    /// No Encryption key has been unlocked
    Locked,
    /// Wrong header type
    WrongHeader {
        /// Expected header type
        expected: &'static str,
        /// Actual header type
        actual: &'static str,
    },
    /// Header failed validation
    HeaderValidation,
    /// Error writing initial nonce
    InitialNonce {
        /// Underlying error
        source: crate::error::KeyError,
    },
    /// Error encoding an action
    ActionEncode {
        /// Underlying error
        source: crate::error::ActionError,
    },
    /// Error decoding an action
    ActionDecode {
        /// The underlying error
        source: crate::error::ActionError,
    },
    /// Error encoding an action during initial setup
    ActionEncodeSetup {
        /// Underlying error
        source: crate::error::ActionError,
    },
    /// Action failed verification
    ActionVerifiy,
    /// Error while trying to seek within the underlying file
    SeekError {
        /// The underlying error
        source: io::Error,
    },
    /// No Nonce Handler set
    NoNonceHandler,
    /// Error performing an encryption during append operation
    AppendEncryption {
        /// The underyling source
        source: crate::error::CipherError,
    },
    /// Other Cipher error
    #[snafu(display("Other cipher error: {}", description))]
    OtherCipher {
        /// The underlying error
        source: crate::error::CipherError,
        /// The error type
        description: &'static str,
    },
    /// Offset beyond end of keystream
    OffsetBeyondEndOfKeystream {
        /// The underlying error
        source: std::num::TryFromIntError,
    },
    /// Offset not in file
    OffsetNotInFile,
    /// Error generating a new nonce
    NonceGeneration {
        /// The source of the error
        source: crate::error::KeyError,
    },
    /// Error flushing output
    Flush {
        /// The underlying error
        source: io::Error,
    },
    /// Tried to overwrite existing data
    UnsupportedOverwrite,
    /// Chunk size too large to fit into memory
    ChunkSizeTooLarge {
        /// The underlying conversion error
        source: std::num::TryFromIntError,
    },
}

/// Encrypting, authenticating wrapper around a [`File`](std::fs::File)-like object (anything that
/// implements [`Read`] + [`Write`] + [`Seek`])
#[derive(Debug)]
pub struct EncryptedFile<T> {
    /// The underlying [`File`](std::fs::File)-like object
    file: T,
    /// The header for the file
    header: Header,
    /// The [`Key`], if there is any
    key: Option<Key>,
    /// The [`MasterKey`] if there is any
    master_key: Option<MasterKey>,
    /// The [`NonceHandler`]
    nonce_handler: NonceHandler,
    /// Start locations and lengths of the actions that commit chunks of bytes
    bytes_locations: Vec<(u64, u32)>,
    /// The locations of every node
    node_locations: HashMap<HmacTag, u64>,
    /// The parents of every node
    node_parents: HashMap<HmacTag, HashSet<HmacTag>>,
    /// This list of roots and their parents, in cronological order
    roots: Vec<HmacTag>,
    /// The list of locations of the roots
    root_locations: HashMap<HmacTag, u64>,
    /// Previously written HmacTag
    previous_tag: HmacTag,
    /// The virtual seek position in the cleartext of this `EncryptedFile`
    cleartext_position: u64,
}

impl<T> EncryptedFile<T>
where
    T: Read + Write + Seek + Debug,
{
    /// Performs initialization operations
    ///
    /// - Do initial `SetNonce` and write it to the file
    ///
    /// # Errors
    ///
    /// sometimes
    #[instrument(err)]
    fn inital_setup(&mut self) -> Result<(), EncryptedFileError> {
        // Generate a nonce
        let nonce = Nonce::random().context(InitialNonceSnafu)?;
        // set the nonce_handler properly
        self.nonce_handler = NonceHandler::new(nonce.clone());
        // make the SetNonce
        let set_nonce = ActionType::SetNonce {
            nonce: nonce.clone(),
        };
        // Make the action
        let action = Action::new(set_nonce, self.key()?).context(ActionEncodeSetupSnafu)?;
        debug!(?action, "Encoding inital action to file");
        // get location in file and insert the nonce into the handler
        let pos = self.file.seek(SeekFrom::Current(0)).context(SeekSnafu)?;
        self.nonce_handler.insert(pos, nonce);
        // Encode the action
        action
            .encode(&mut self.file)
            .context(ActionEncodeSetupSnafu)?;
        Ok(())
    }

    /// Makes the correct internal state changes in response to an action
    ///
    /// Must only be called on actions which have been verified
    fn accept_action(&mut self, position: u64, action: Action<'_>) {
        match action.action {
            ActionType::SetNonce { nonce } => self.nonce_handler.insert(position, nonce),
            ActionType::Bytes { payload } => self.bytes_locations.push((
                position,
                payload
                    .len()
                    .try_into()
                    .expect("ActionType::Bytes had an impossibly long payload "),
            )),
            ActionType::Node { left, right } => {
                // Mark down the node location
                self.node_locations.insert(action.tag, position);
                // Update parentage information
                self.node_parents
                    .entry(left)
                    .or_default()
                    .insert(action.tag);
                self.node_parents
                    .entry(right)
                    .or_default()
                    .insert(action.tag);
            }
            ActionType::Root { new_root, .. } => {
                // Insert the root into the list
                self.roots.push(new_root);
                // Insert the location into the root locations list
                self.root_locations.insert(new_root, position);
            }
        }
    }

    /// Parses and applies actions.
    ///
    /// This will parse the locations of all chunks of bytes within the file, as well as the
    /// locations and values of all Nodes in the merkel tree, but *will not* verify the tree.
    ///
    /// # Errors
    ///
    /// Sometimes
    #[instrument(err)]
    fn parse_actions(&mut self) -> Result<(), EncryptedFileError> {
        // Seek to end to get the length
        // TODO: Clean this up, abstract it into a trait. This would potentially be not very good on
        // a tape drive
        trace!("Finding length of file");
        let end = self.file.seek(SeekFrom::End(0)).context(SeekSnafu)?;
        // Go back to the beginning of the file
        trace!("Seeking back to start of file");
        self.file.seek(SeekFrom::Start(0)).context(SeekSnafu)?;
        trace!("Re-reading header to get into position for first action");
        // Read the header, to get us into position
        let _header = Header::decode(&mut self.file).context(HeaderDecodeSnafu)?;
        let mut position = self.inner_position()?;
        trace!(?position, "Starting read after header");
        // Ensure the nonce_handler is fresh
        self.nonce_handler = NonceHandler::new(Nonce::zero());
        // Read off the actions
        while position < end {
            debug!(?position, "Attempting to parse action");
            // Decode the action
            let action = Action::decode(&mut self.file).context(ActionDecodeSnafu)?;
            // Validate the action
            if !action.verify(self.key()?).context(ActionDecodeSnafu)? {
                error!(?action, "Action failed verification");
                return ActionVerifiySnafu.fail();
            }
            // Update our last seen hmac tag
            //
            // It is important that we do this after valding the action, to categorically reject
            // corrupt/invalid data.
            self.previous_tag = action.tag;
            // Perform the relevant thing for each action
            debug!(?action, "Parsing Action");
            self.accept_action(position, action);
            // Update the position
            position = self.inner_position()?;
        }
        Ok(())
    }

    /// Creates a new `EncryptedFile` with a derived key
    ///
    /// # Errors
    ///
    /// Will return an error if the RNG fails during key derivation, or if an I/O error occurs while
    /// writing to the file
    #[instrument(err)]
    pub fn create_derived(mut file: T, master_key: &MasterKey) -> Result<Self, EncryptedFileError> {
        info!("Creating a new file with a derived key");
        trace!("Generating derived key");
        let (key, mut uuid) = master_key.derive().context(DeriveSnafu)?;
        trace!("GEnerating a new nonce");
        let nonce = Nonce::random().context(DeriveSnafu)?;
        trace!("Encrypting UUID of derived key for file");
        let mut cipher = Cipher::new(&master_key, nonce.clone());
        cipher
            .apply_keystream(&mut uuid)
            .context(OtherCipherSnafu {
                description: "Encrypting UUID",
            })?;
        trace!("Generating HmacTag for UUID of derived key");
        let hmac = Hmac::new(&master_key);
        let mut buffer = uuid.to_vec();
        buffer.extend(nonce.nonce_bytes());
        let tag = hmac.tag(&buffer);
        let hmac: [u8; 32] = *tag.tag.as_ref();
        trace!("Assembling header");
        let header = Header::DerivedKey {
            encrypted_uuid: uuid,
            nonce,
            hmac,
            // 16 kiB
            chunk_size: 16 * 1024,
        };
        trace!("Encoding header");
        header.encode(&mut file).context(HeaderEncodeSnafu)?;

        let mut ret = Self {
            file,
            header,
            key: Some(key),
            master_key: None,
            nonce_handler: NonceHandler::new(Nonce::zero()),
            bytes_locations: Vec::new(),
            node_locations: HashMap::new(),
            node_parents: HashMap::new(),
            roots: Vec::new(),
            root_locations: HashMap::new(),
            previous_tag: HmacTag::zero(),
            cleartext_position: 0,
        };
        debug!("Performing inital setup for new file");
        ret.inital_setup()?;
        Ok(ret)
    }

    /// Opens an existing `EncryptedFile` with a derived key
    ///
    /// # Errors
    ///
    /// Will return an error if there is an I/O error while reading from the file, if the header is
    /// the wrong type for this method, or if the header fails verification
    #[instrument(err)]
    pub fn open_derived(mut file: T, master_key: &MasterKey) -> Result<Self, EncryptedFileError> {
        info!("Opening a file with a derived key");
        trace!("Decoding header");
        let header = Header::decode(&mut file).context(HeaderDecodeSnafu)?;
        debug!(?header);
        match &header {
            Header::StoredKey { .. } => {
                error!("Encountered wrong header type, expecting DerivedKey but found StoredKey");
                WrongHeaderSnafu {
                    expected: "DerivedKey",
                    actual: "StoredKey",
                }
                .fail()
            }
            Header::DerivedKey {
                encrypted_uuid,
                nonce,
                hmac,
                chunk_size: _,
            } => {
                let mut uuid = *encrypted_uuid;
                trace!("Validating the HmacTag of the derived key");
                let hmacer = Hmac::new(master_key);
                let mut buffer = encrypted_uuid.to_vec();
                buffer.extend(nonce.nonce_bytes());
                let output_tag = hmacer.tag(&buffer);
                let input_tag = HmacTag::from(hmac);
                ensure!(output_tag == input_tag, HeaderValidationSnafu);
                trace!("Decrypting the HmacTag of the derived key");
                let mut cipher = Cipher::new(master_key, nonce.clone());
                cipher
                    .apply_keystream(&mut uuid)
                    .context(OtherCipherSnafu {
                        description: "Decrypting derived key uuid",
                    })?;
                debug!("Rederiving the derived key from the master key");
                let key = master_key.derive_again(uuid);

                let mut ret = Self {
                    file,
                    header,
                    key: Some(key),
                    master_key: None,
                    nonce_handler: NonceHandler::new(Nonce::zero()),
                    bytes_locations: Vec::new(),
                    node_locations: HashMap::new(),
                    node_parents: HashMap::new(),
                    roots: Vec::new(),
                    root_locations: HashMap::new(),
                    previous_tag: HmacTag::zero(),
                    cleartext_position: 0,
                };
                debug!("Performing inital action parsing for loaded file");
                ret.parse_actions()?;
                Ok(ret)
            }
        }
    }

    /// Creates a new `EncryptedFile` with a master key
    ///
    /// # Errors
    ///
    /// Will return an error if the RNG faults during key generation, or if there is an I/O error
    /// writing to the file
    #[instrument(err, skip(password))]
    pub fn create_stored(
        mut file: T,
        password: &[u8],
        chunk_size: u64,
    ) -> Result<Self, EncryptedFileError> {
        trace!("Generating master key");
        let master_key = MasterKey::random().context(MasterKeyGenSnafu)?;
        trace!("Encrypting master key");
        let encrypted_key = PasswordEncryptedMasterKey::encrypt(&master_key, password)
            .context(MasterKeyGenSnafu)?;
        trace!("Assembling header");
        let header = Header::StoredKey {
            encrypted_key,
            chunk_size,
        };
        debug!(?header, "Writing header");
        header.encode(&mut file).context(HeaderEncodeSnafu)?;
        let mut ret = Self {
            file,
            header,
            key: None,
            master_key: Some(master_key),
            nonce_handler: NonceHandler::new(Nonce::zero()),
            bytes_locations: Vec::new(),
            node_locations: HashMap::new(),
            node_parents: HashMap::new(),
            roots: Vec::new(),
            root_locations: HashMap::new(),
            previous_tag: HmacTag::zero(),
            cleartext_position: 0,
        };
        debug!("Preforming initial setup");
        ret.inital_setup()?;
        Ok(ret)
    }

    /// Opens an existing `EncryptedFile` with a master key
    ///
    /// # Errors
    ///
    /// Will return an error if there is an I/O error while reading from the file, if the header is
    /// the wrong type for this method, or if the header fails verification
    #[instrument(err, skip(password))]
    pub fn open_stored(mut file: T, password: &[u8]) -> Result<Self, EncryptedFileError> {
        trace!("Decoding header");
        let header = Header::decode(&mut file).context(HeaderDecodeSnafu)?;
        debug!(?header, "Decoded header, checking for correct variant");
        // Make sure its the correct variant
        match &header {
            Header::StoredKey { encrypted_key, .. } => {
                debug!("Attempting to decrypt key");
                let master_key = encrypted_key
                    .decrypt(password)
                    .context(MasterKeyDecryptSnafu)?;
                trace!("Decrypted master key");
                let mut ret = Self {
                    file,
                    header,
                    key: None,
                    master_key: Some(master_key),
                    nonce_handler: NonceHandler::new(Nonce::zero()),
                    bytes_locations: Vec::new(),
                    node_locations: HashMap::new(),
                    node_parents: HashMap::new(),
                    roots: Vec::new(),
                    root_locations: HashMap::new(),
                    previous_tag: HmacTag::zero(),
                    cleartext_position: 0,
                };
                debug!("Performing inital parse");
                ret.parse_actions()?;
                Ok(ret)
            }
            Header::DerivedKey { .. } => WrongHeaderSnafu {
                expected: "StoredKey",
                actual: "DerivedKey",
            }
            .fail(),
        }
    }

    /// Returns the currently in use key
    ///
    /// # Errors
    ///
    /// Will error if the `EncryptedFile` does not have any keys loaded
    #[instrument]
    pub fn key(&self) -> Result<&Key, EncryptedFileError> {
        // It is invalid to have two different keys loaded
        debug_assert!(!(self.key.is_some() && self.master_key.is_some()));
        if let Some(key) = self.key.as_ref() {
            Ok(key)
        } else if let Some(master_key) = self.master_key.as_ref() {
            Ok(master_key.as_ref())
        } else {
            error!("Did not have any keys loaded");
            LockedSnafu.fail()
        }
    }

    /// Returns the master key, if there is one
    pub fn master_key(&self) -> Option<&MasterKey> {
        self.master_key.as_ref()
    }

    /// Flushes any internal buffers
    ///
    /// # Errors
    ///
    /// Will return an error if any underlying IO errors occur
    #[allow(clippy::unused_self)]
    pub fn flush(&mut self) -> Result<(), EncryptedFileError> {
        // TODO: Implement more stuff
        // Flush the underlying file
        self.file.flush().context(FlushSnafu)?;
        Ok(())
    }

    /// Consumes the file and returns the inner file
    pub fn into_inner(self) -> T {
        // TODO: flush if needed, and deal with the probable future need for a Drop impl
        self.file
    }

    /// Returns a reference to the header
    pub fn header(&self) -> &Header {
        &self.header
    }

    /// Returns the current offset within the inner file
    ///
    /// # Errors
    ///
    /// Will return an error if the internal [`Seek`] operation fails
    pub fn inner_position(&mut self) -> Result<u64, EncryptedFileError> {
        let pos = self.file.seek(SeekFrom::Current(0)).context(SeekSnafu)?;
        Ok(pos)
    }

    /// Returns the apparent lenght of the plaintext content
    pub fn apparent_length(&self) -> u64 {
        // Add up the lengths of all the bytes entries
        let mut total_length: u64 = 0;
        for (_, len) in &self.bytes_locations {
            total_length += u64::from(*len);
        }
        total_length
    }

    /// Appends an [`ActionType`] to the end of this file, doing all nessicary handling
    ///
    /// Returns the [`HmacTag`] of the resulting action
    ///
    /// # Errors
    ///
    /// Will propagate any underlying IO errors that occur
    #[instrument(skip(action), err)]
    pub fn append_action(&mut self, action: ActionType<'_>) -> Result<HmacTag, EncryptedFileError> {
        // Convert into an action
        let key = self.key()?;
        let action = Action::new(action, key).context(ActionEncodeSnafu)?;
        // Go to the end of the file and record the position
        let position = self.file.seek(SeekFrom::End(0)).context(SeekSnafu)?;
        // Wrtie the action
        action.encode(&mut self.file).context(ActionEncodeSnafu)?;
        // Now perform whatever opertation needs done for this type of action
        let tag = action.tag;
        self.accept_action(position, action);

        Ok(tag)
    }

    /// Appends a chunk of bytes to the end of this file
    ///
    /// This will:
    ///  - Encrypt the bytes with the correct nonce and offset for the current end of the file,
    ///    creating a new nonce if needed
    ///  - Put those bytes in a [`ActionType::Bytes`]
    ///  - Create a new [`ActionType::Node`] using a naive/suboptimal, but working strategy
    ///  - Create a new [`ActionType::Root`] pointing to the created node
    ///
    /// In the interest of avoiding uneeded copies, this method takes ownership of a `Vec<u8>`, so
    /// any needed copies must be handled by the consumer.
    /// # Errors
    ///
    /// Will propagate any underlying IO errors that occur
    #[instrument(skip(bytes), err)]
    pub fn append_bytes(&mut self, mut bytes: Vec<u8>) -> Result<(), EncryptedFileError> {
        // Get the real position so we can lookup the nonce
        let mut real_start_offset =
            self.file.seek(SeekFrom::End(0)).context(SeekSnafu)? + BYTES_PAYLOAD_OFFSET;
        let length = u32::try_from(bytes.len()).context(OffsetBeyondEndOfKeystreamSnafu)?;
        // Get nonce and keystream position
        let (mut nonce, mut keystream_position) = self.nonce_handler.lookup(real_start_offset);

        // Check and see if we need to write a new nonce
        //
        // Our target value for nonce reset here is set to be 2^32 bytes, so we can easily check by
        // seeing if we'll send the keystream position over u32::max
        if keystream_position + u64::from(length) > u64::from(u32::MAX) {
            debug!("Nonce ran out, creating a new one");
            // Load up a new nonce
            nonce = Nonce::random().context(NonceGenerationSnafu)?;

            // Write it to the file and perform processing
            let action_type = ActionType::SetNonce {
                nonce: nonce.clone(),
            };
            self.append_action(action_type)?;

            // Call things up again to get our new keystream position and verify the nonce
            real_start_offset = self.file.seek(SeekFrom::End(0)).context(SeekSnafu)?;
            let (test_nonce, new_keystream_position) = self.nonce_handler.lookup(real_start_offset);
            // Make sure it gives us back the correct nonce
            debug_assert_eq!(test_nonce, nonce);
            keystream_position = new_keystream_position;
        }

        // Perform the encryption
        let mut cipher = Cipher::new(self.key()?, nonce);
        cipher.seek(u32::try_from(keystream_position).context(OffsetBeyondEndOfKeystreamSnafu)?);
        cipher
            .apply_keystream(&mut bytes)
            .context(AppendEncryptionSnafu)?;

        // Package up and write out the bytes
        let action_type = ActionType::Bytes {
            payload: bytes.into(),
        };
        let right = self.append_action(action_type)?;

        // Create the node and write it out
        let left: HmacTag = self
            .node_locations
            .iter()
            .max_by(|(_, a), (_, b)| a.cmp(b))
            .map_or_else(HmacTag::zero, |(x, _)| *x);
        let action_type = ActionType::Node { left, right };
        let new_root = self.append_action(action_type)?;

        // Create the new root and write it out
        let previous_root = self.roots.last().copied().unwrap_or_else(HmacTag::zero);
        let action_type = ActionType::Root {
            new_root,
            previous_root,
        };
        self.append_action(action_type)?;

        Ok(())
    }

    /// Gets the "real" position of a byte in the file given its apparent position in the clear text
    fn apparent_to_real(&self, apparent: u64) -> Result<u64, EncryptedFileError> {
        // Special case the 0 byte
        if apparent == 0 {
            return if let Some(first_bytes) = self.bytes_locations.first() {
                Ok(first_bytes.0 + BYTES_PAYLOAD_OFFSET)
            } else {
                OffsetNotInFileSnafu.fail()
            };
        }
        // Return
        // Find the location of the byte within the file
        let mut real_position = 0;
        let mut offset = apparent;
        let mut bytes_passed = 0;
        for (position, length) in self
            .bytes_locations
            .iter()
            .copied()
            .map(|(x, y)| (x + BYTES_PAYLOAD_OFFSET, y))
        {
            // Make sure we can still plausably find the byte
            if bytes_passed > apparent {
                return OffsetNotInFileSnafu.fail();
            }
            // Check to see if we are in this segement
            if offset < u64::from(length) {
                real_position = offset + position;
                break;
            }
            // Otherwise update our values
            bytes_passed += u64::from(length);
            offset -= u64::from(length);
        }
        Ok(real_position)
    }

    /// Debugging method, reparses the file and dumps the parse
    ///
    /// # Panics
    ///
    /// a lot
    pub fn debug(&mut self) -> String {
        let mut output = String::new();
        // Seek back to start
        self.file.seek(SeekFrom::Start(0)).expect("Failed to seek");
        // Decode a header
        let header: Header = Header::decode(&mut self.file).unwrap();
        output.extend(format!("{:?} - {}\n", header, header.length()).chars());
        while let Ok(action) = Action::decode(&mut self.file) {
            output.extend(format!("{:?} - {}\n", action, action.length()).chars());
        }

        output
    }

    /// Returns the chunk size defined in this file's header
    pub fn chunk_size(&self) -> u64 {
        self.header.chunk_size()
    }
}

impl<T> Read for EncryptedFile<T>
where
    T: Read + Write + Seek + Debug,
{
    /// Read some bytes from the file, decrypting on the fly
    ///
    /// TODO: Check chunks we encounter for validity
    #[instrument(skip(self, buf), level = "trace")]
    fn read(&mut self, buf: &mut [u8]) -> io::Result<usize> {
        // If the apparent position is past the end of the file, then signal that
        if self.cleartext_position >= self.apparent_length() {
            return Ok(0);
        }
        // Find the "real" position of our cipher text bytes in
        let real_position = self
            .apparent_to_real(self.cleartext_position)
            .map_err(|e| io::Error::new(io::ErrorKind::UnexpectedEof, e))?;
        // Seek to our real position
        self.file.seek(SeekFrom::Start(real_position))?;
        // Find our nonce next
        let (nonce, keystream_position) = self.nonce_handler.lookup(real_position);
        // Determine how many bytes we are reading, first find the relevant bytes entry in our vec
        //
        // The offset returned by apparent_to_real includes the offset after the bytes header, so
        // this call will always error, as this list has the offsets of the starts of the headers
        let bytes_index = self
            .bytes_locations
            .binary_search_by_key(&real_position, |(x, _)| *x)
            .map_err(|x| x.saturating_sub(1))
            .unwrap_err();

        // It is safe to unconditionally index into the array now, as the previous call to
        // apparent_to_real would have errored if there wasn't a matching index.
        let (start_location, length) = self.bytes_locations[bytes_index];
        // Convert the length to a u64 so we can do math on it
        let length = u64::from(length);
        // Now check to see if we can read all the bytes we want out
        let available_length = (start_location + length + BYTES_PAYLOAD_OFFSET) - real_position;
        let available_length: usize = available_length
            .try_into()
            .map_err(|e| io::Error::new(io::ErrorKind::OutOfMemory, e))?;
        // Trim the buffer if the consumer is trying to take too much
        let buf = if buf.len() <= available_length {
            buf
        } else {
            // Peel off as much of the buffer as we can take
            &mut buf[..available_length]
        };
        // We can do it all in one go now that we have trimmed the buffer
        //
        // Read the cipher text bytes into the buffer
        self.file.read_exact(buf)?;
        // Update our internal position
        self.cleartext_position += buf.len() as u64;
        // Decrypt the bytes
        //
        // This is safe to do all in one go as it is a format error for a single bytes to
        // contain an illegally long key stream
        //
        // We also get the key late here, to avoid angering the borrow checker
        let key = self
            .key()
            .map_err(|e| io::Error::new(io::ErrorKind::Unsupported, e))?;
        let mut cipher = Cipher::new(key, nonce);
        cipher.seek(
            keystream_position
                .try_into()
                .map_err(|e| io::Error::new(io::ErrorKind::Other, e))?,
        );
        // Do the decryption in place
        let buffer_len = buf.len();
        cipher
            .apply_keystream(buf)
            .map_err(|e| io::Error::new(io::ErrorKind::Other, e))?;
        // Return the length of the buffer
        Ok(buffer_len)
    }
}

impl<T> Seek for EncryptedFile<T>
where
    T: Read + Write + Seek + Debug,
{
    #[instrument(skip(self), level = "trace")]
    fn seek(&mut self, pos: SeekFrom) -> io::Result<u64> {
        // TODO: Add some sanity checking around the seek position
        match pos {
            SeekFrom::Start(x) => {
                self.cleartext_position = x;
                Ok(x)
            }
            SeekFrom::End(x) => {
                let cleartext_i64: i64 = self
                    .apparent_length()
                    .try_into()
                    .map_err(|e| io::Error::new(io::ErrorKind::InvalidInput, e))?;
                let new_position: u64 = (cleartext_i64 + x)
                    .try_into()
                    .map_err(|e| io::Error::new(io::ErrorKind::InvalidInput, e))?;
                self.cleartext_position = new_position;
                Ok(new_position)
            }
            SeekFrom::Current(x) => {
                let cleartext_i64: i64 = self
                    .cleartext_position
                    .try_into()
                    .map_err(|e| io::Error::new(io::ErrorKind::InvalidInput, e))?;
                let new_position: u64 = (cleartext_i64 + x)
                    .try_into()
                    .map_err(|e| io::Error::new(io::ErrorKind::InvalidInput, e))?;
                self.cleartext_position = new_position;
                Ok(new_position)
            }
        }
    }
}

impl<T> Write for EncryptedFile<T>
where
    T: Read + Write + Seek + Debug,
{
    #[instrument(skip(self, buf), level = "trace")]
    fn write(&mut self, buf: &[u8]) -> io::Result<usize> {
        if self.cleartext_position == self.apparent_length() {
            // See if we are trying to write more than a chunk_size
            let chunk_size: usize = usize::try_from(self.chunk_size())
                .context(ChunkSizeTooLargeSnafu)
                .map_err(|e| io::Error::new(io::ErrorKind::Other, e))?;
            let bytes_written = if buf.len() > chunk_size {
                // We need to peel off a chunk_size and write it
                let buf = &buf[0..chunk_size];
                self.append_bytes(buf.to_vec())
                    .map_err(|e| io::Error::new(io::ErrorKind::Other, e))?;
                buf.len()
            } else {
                self.append_bytes(buf.to_vec())
                    .map_err(|e| io::Error::new(io::ErrorKind::Other, e))?;
                buf.len()
            };
            self.seek(SeekFrom::Current(
                bytes_written
                    .try_into()
                    .map_err(|e| io::Error::new(io::ErrorKind::Other, e))?,
            ))?;
            Ok(bytes_written)
        } else {
            Err(io::Error::new(
                io::ErrorKind::Unsupported,
                UnsupportedOverwriteSnafu.build(),
            ))
        }
    }

    fn flush(&mut self) -> io::Result<()> {
        match self.flush() {
            Ok(_) => Ok(()),
            Err(e) => {
                if let EncryptedFileError::Flush { source } = e {
                    Err(source)
                } else {
                    Err(io::Error::new(io::ErrorKind::Other, e))
                }
            }
        }
    }
}

#[cfg(test)]
mod tests {
    use super::*;
    use proptest::prelude::*;
    use std::io::{Cursor, SeekFrom};
    mod smoke {
        use super::*;
        // Test opening and closing an `EncryptedFile` with a derived key
        #[test]
        fn open_close_derived() {
            let master_key = MasterKey::random().unwrap();
            let buffer = Cursor::new(Vec::new());
            // Create the file
            let enc_file = EncryptedFile::create_derived(buffer, &master_key).unwrap();
            let key_one: Key = enc_file.key().unwrap().clone();
            // Close the file
            let mut buffer = enc_file.into_inner();
            buffer.seek(SeekFrom::Start(0)).unwrap();
            // Reopen the file
            let enc_file = EncryptedFile::open_derived(buffer, &master_key).unwrap();
            // Verify the key equality
            let key_two = enc_file.key().unwrap().clone();
            assert_eq!(key_one.cipher.key_bytes(), key_two.cipher.key_bytes());
            assert_eq!(key_one.hmac.key_bytes(), key_two.hmac.key_bytes());
        }
        // Test opening and closing an `EncryptedFile` with a derived key
        #[test]
        fn open_close_stored() {
            let password = "Totally a password".as_bytes();
            // make a buffer
            let buffer = Cursor::new(Vec::new());
            // Create an `EncryptedFile`
            let file = EncryptedFile::create_stored(buffer, password, 16 * 1024).unwrap();
            let master_key = file.master_key().unwrap().clone();
            // Get the buffer back out and reset to start
            let mut buffer = file.into_inner();
            buffer.seek(SeekFrom::Start(0)).unwrap();
            // Read the EncryptedFile back out
            let file = EncryptedFile::open_stored(buffer, password).unwrap();
            let new_master_key = file.master_key().unwrap().clone();
            // Verify that we have the same keys
            assert_eq!(
                master_key.key.cipher.key_bytes(),
                new_master_key.key.cipher.key_bytes()
            );
            assert_eq!(
                master_key.key.hmac.key_bytes(),
                new_master_key.key.hmac.key_bytes()
            );
            assert_eq!(&master_key.noise, &new_master_key.noise);
        }
        // Test writing and reading some data on a stored key in a cursor
        #[test]
        fn stored_round_trip_cursor() {
            let password = "Totally a password".as_bytes();
            // make a buffer
            let buffer = Cursor::new(Vec::new());
            // Create an `EncryptedFile`
            let mut file = EncryptedFile::create_stored(buffer, password, 16 * 1024).unwrap();
            // Some data to write in
            let data = vec![1_u8; 64 * 1024];
            // Write the data in
            file.write_all(&data).unwrap();
            println!("Bytes before {:?}", file.bytes_locations);
            println!("Nonces before {:?}", file.nonce_handler.map);
            // Flush file
            file.flush().unwrap();

            // Seek back to start and read back out
            println!("\nSeeking back to start\n");
            // Seek back to start of file
            file.seek(SeekFrom::Start(0)).unwrap();
            let mut output = vec![];
            file.read_to_end(&mut output).unwrap();
            assert_eq!(output.len(), data.len());
            assert!(output == data);

            // Reload the file
            println!("\nReloading File\n");
            let mut buffer = file.into_inner();
            buffer.seek(SeekFrom::Start(0)).unwrap();
            // Read the EncryptedFile back out
            let mut file = EncryptedFile::open_stored(buffer, password).unwrap();
            println!("Bytes after {:?}", file.bytes_locations);
            println!("Nonces after {:?}", file.nonce_handler.map);
            // Seek back to start of file
            file.seek(SeekFrom::Start(0)).unwrap();
            let mut output = vec![];
            file.read_to_end(&mut output).unwrap();
            assert_eq!(output.len(), data.len());
            assert!(output == data);
        }
    }
    mod end_to_end {
        use super::*;
        const MAX_SEGMENTS: usize = 32;
        const CHUNK_LEN: u64 = 4 * 1024;
        fn vec_of_vec() -> impl Strategy<Value = Vec<Vec<u8>>> {
            let length = 0..MAX_SEGMENTS;
            length.prop_flat_map(vec_from_length)
        }
        #[allow(clippy::cast_possible_truncation)]
        fn vec_from_length(max_segments: usize) -> impl Strategy<Value = Vec<Vec<u8>>> {
            let mut result = vec![];
            for _ in 1..max_segments {
                result.push(prop::collection::vec(0..=u8::MAX, 0..(CHUNK_LEN as usize)));
            }
            result
        }

        proptest! {
            #![proptest_config(ProptestConfig::with_cases(50))]
            // In memory round trip of simulated files
            #[test]
            fn round_trip_segemented(input in vec_of_vec()) {
                let data: Vec<u8> = input.iter().flatten().copied().collect();
                let password = "Totally a password".as_bytes();
                // make a buffer
                let buffer = Cursor::new(Vec::new());
                // Create an `EncryptedFile`
                let mut file = EncryptedFile::create_stored(buffer, password, CHUNK_LEN).unwrap();
                // Write the data in
                for segment in input {
                    file.write_all(&segment).unwrap();
                }
                // Flush the file
                file.flush().unwrap();
                // Reload the file
                let mut buffer = file.into_inner();
                buffer.seek(SeekFrom::Start(0)).unwrap();
                // Read the EncryptedFile back out
                let mut file = EncryptedFile::open_stored(buffer, password).unwrap();
                // Seek back to start of file
                file.seek(SeekFrom::Start(0)).unwrap();
                let mut output = vec![];
                file.read_to_end(&mut output).unwrap();
                assert_eq!(output.len(), data.len());
                assert!(output == data);
            }
            #[test]
            fn round_trip_all_in_one(input in vec_of_vec()) {
                let data: Vec<u8> = input.iter().flatten().copied().collect();
                let password = "Totally a password".as_bytes();
                // make a buffer
                let buffer = Cursor::new(Vec::new());
                // Create an `EncryptedFile`
                let mut file = EncryptedFile::create_stored(buffer, password, CHUNK_LEN).unwrap();
                // Write the data in
                file.write_all(&data).unwrap();
                // Flush the file
                file.flush().unwrap();
                // Reload the file
                let mut buffer = file.into_inner();
                buffer.seek(SeekFrom::Start(0)).unwrap();
                // Read the EncryptedFile back out
                let mut file = EncryptedFile::open_stored(buffer, password).unwrap();
                // Seek back to start of file
                file.seek(SeekFrom::Start(0)).unwrap();
                let mut output = vec![];
                file.read_to_end(&mut output).unwrap();
                assert_eq!(output.len(), data.len());
                assert!(output == data);
            }
        }
    }
}
