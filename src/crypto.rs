//! Basic Cryptography Primitives <span style="color:red">**HAZMAT**</span>
//!
//! This module provides low level types for representing cryptographic keys and performing
//! cryptographic operations, in particular, this module provides the primitives to implement the
//! `XChaCha20` + `Blake3` AEAD scheme.
//!
//! # <span style="color:red">**DANGER**</span>
//!
//! This module deals in low level cryptographic details, and it can be incredibly dangerous to
//! interact with directly. It is advisable to not deal with this module directly, and instead use a
//! higher level API.
//!

/// Cipher types
pub(crate) mod cipher;
/// HMAC types
pub(crate) mod hmac;
/// Key types
pub(crate) mod key;

pub use cipher::Cipher;
pub use hmac::{Hmac, HmacTag, NamespacedTag};
pub use key::{CipherKey, HmacKey, Key, MasterKey, Nonce, PasswordEncryptedMasterKey};
