//! Basic, but secure random number generation
//!
//! Uses [`XChaCha20`] to extend each 56 bytes of entropy to around 8kiB of output
use std::cell::RefCell;

use chacha20::{
    cipher::{generic_array::GenericArray, KeyIvInit, StreamCipher},
    XChaCha20,
};
use snafu::{ResultExt, Snafu};
use tracing::{debug, instrument, trace};

/// Error type
#[derive(Snafu, Debug)]
#[non_exhaustive]
pub enum RandError {
    /// Failure with getrandom
    GetRandom {
        /// Underlying error
        source: getrandom::Error,
    },
    /// Inconsistent state, cipher was not configured
    NoCipher,
    /// Cipher reached end of keystream
    KeyStream {
        /// Underlying error
        source: chacha20::cipher::StreamCipherError,
    },
}

/// Generates a random slice of bytes, using an xchacha20 cipher that is continuously reseed with
/// calls to `getrandom`
///
/// # Errors
///
/// Will error if there is a cipher failure, or if the call to `getrandom` fails
#[allow(clippy::missing_panics_doc)]
#[instrument(skip(buf), err)] // Can't actually panic
pub fn rand(buf: &mut [u8]) -> Result<(), RandError> {
    thread_local! {
        static CIPHER_KEY: RefCell<[u8; 32]> = RefCell::new([0_u8; 32]);
        static CIPHER_NONCE: RefCell<[u8; 24]> = RefCell::new([0_u8; 24]);
        static CIPHER: RefCell<Option<XChaCha20>> = RefCell::new(None);
        static CIPHER_RESET: RefCell<(bool, usize)> = RefCell::new((true, 0));
    }
    trace!("Getting thread-local cipher key");
    CIPHER_KEY.with(|cipher_key| {
        trace!("Getting thread local cipher nonce");
        CIPHER_NONCE.with(|cipher_nonce| {
            trace!("Getting thread local cipher instance");
            CIPHER.with(|cipher| {
                trace!("Getting thread local cipher reset flag");
                CIPHER_RESET.with(|cipher_reset| {
                    trace!("All thread-locals aquired");
                    let mut key = cipher_key.borrow_mut();
                    let mut cipher = cipher.borrow_mut();
                    let mut reset = cipher_reset.borrow_mut();
                    let mut nonce = cipher_nonce.borrow_mut();
                    // Add more random bytes if we need to
                    if reset.0 {
                        debug!("Adding more entropy to pool");
                        let mut new_key = [0_u8; 56];
                        // Get more entropy than we really should need, over the course of several
                        // syscalls. This will improve the chances of, but not guarantee, a
                        // misbehaving random number generator to provide enough entropy to not
                        // cause problems
                        //
                        // This is by far not a bullet proof solution, and the application really
                        // _should_ be able to trust the OS provided random numbers, but this does
                        // give us a little more leeway in the event of an improperly seeded entropy
                        // pool or the like
                        //
                        // TODO: Even more paranoid generation
                        let mut noise = [0_u8; 2048];
                        // Get 32 bytes at a time
                        for chunk in noise.chunks_mut(32) {
                            getrandom::getrandom(chunk).context(GetRandomSnafu)?;
                        }
                        // Feed it into the KDF
                        let mut hasher = blake3::Hasher::new_derive_key(
                            "Replicator Wed Dec  8 02:11:57 AM EST 2021 Random Number Generation",
                        );
                        hasher.update(&noise);
                        // Read it out
                        let mut output = hasher.finalize_xof();
                        output.fill(&mut new_key);
                        // XOR the old key and the new bytes together
                        for (key_byte, new_byte) in
                            key.iter_mut().zip(new_key[..32].iter().copied())
                        {
                            *key_byte ^= new_byte;
                        }
                        // XOR the old nonce and the new nonce together
                        for (nonce_byte, new_byte) in
                            nonce.iter_mut().zip(new_key[32..].iter().copied())
                        {
                            *nonce_byte ^= new_byte;
                        }
                        // Reset the bytes counter to the totally arbitrary 8kiB
                        reset.1 = 8_192;
                        // Replace the cipher
                        let new_cipher = XChaCha20::new(
                            GenericArray::from_slice(&key[..]),
                            GenericArray::from_slice(&nonce[..]),
                        );
                        *cipher = Some(new_cipher);
                    }
                    debug!("Generating psudeorandom bytes");

                    // Safe, since the cipher is always initialized by now
                    let chacha = cipher.as_mut().unwrap();
                    chacha.try_apply_keystream(buf).context(KeyStreamSnafu)?;

                    // Set the reset variables
                    if buf.len() >= reset.1 {
                        reset.0 = true;
                        reset.1 = 0;
                    } else {
                        reset.1 -= buf.len();
                    }

                    Ok(())
                })
            })
        })
    })
}

#[cfg(test)]
mod tests {
    use super::*;
    use std::collections::HashSet;
    // Make sure the RNG
    //     A.) Doesn't error out
    //     B.) Returns us some non-zero numbers
    #[test]
    fn smoke() {
        let mut buffer = [0_u8; 32];
        rand(&mut buffer).expect("Failed to generate random numbers");
        println!("{:02X?}", buffer);
        assert_ne!(buffer, [0_u8; 32]);
    }
    // Generate a bunch of byte tags, and make sure there are no duplicates
    #[test]
    fn no_duplicates() {
        let mut tags = HashSet::new();
        // Generate 100 tags of length 500
        for _ in 0..100 {
            let mut buffer = [0_u8; 500];
            rand(&mut buffer).expect("Failed to generate random numbers");
            tags.insert(buffer);
        }
        assert_eq!(tags.len(), 100);
    }
}
