use redacted::FullyRedacted;
use snafu::{ResultExt, Snafu};
use zeroize::Zeroize;

use crate::{file::traits::Encode, rand::rand};

/// Internal module for encrypted master key types
mod encrypted;

pub use encrypted::PasswordEncryptedMasterKey;

/// Key Conversion Error
#[derive(Debug, Snafu)]
#[non_exhaustive]
pub enum KeyError {
    /// A key with an incorrect length was provided
    #[snafu(display(
        "{} had incorrect length. expected: {} actual: {}",
        key_type,
        expected,
        actual
    ))]
    IncorrectLength {
        /// The type of key
        key_type: &'static str,
        /// The expected length
        expected: usize,
        /// The actual length
        actual: usize,
    },
    /// An error occured in the RNG during key generation
    #[snafu(display("Failed generating a {}, failure in RNG", key_type))]
    Generation {
        /// The type being generated
        key_type: &'static str,
        /// The underlying error
        source: crate::rand::RandError,
    },
    /// An error occurred while hashing the user provided password
    Password {
        /// The underlying error
        source: balloon_hash::Error,
    },
    /// HMAC Verification of encrypted key failed (check password)
    Verification,
    /// An I/O Error occured encoding/decoding a master key
    IO {
        /// The underlying error
        source: std::io::Error,
    },
    /// An Error occurred encoding/decoding a nonce
    NonceEnc {
        /// The underlying error
        source: crate::error::SliceError,
    },
}

/// A key used for encryption
#[derive(Zeroize, Debug, Clone)]
#[zeroize(drop)]
pub struct CipherKey {
    /// The key proper
    pub(crate) key: FullyRedacted<[u8; 32]>,
}

impl CipherKey {
    /// Returns the bytes of the key, as a slice
    pub fn key_bytes(&self) -> &[u8] {
        self.key.as_ref()
    }
    /// Returns the bytes of the key, as an array reference
    pub fn key_bytes_array(&self) -> &[u8; 32] {
        self.key.as_ref()
    }
    /// Generate a random [`CipherKey`]
    ///
    /// # Errors
    ///
    /// Will return an error if the random number generator faults during generation
    pub fn random() -> Result<CipherKey, KeyError> {
        let mut buffer = [0_u8; 32];
        rand(&mut buffer).context(GenerationSnafu {
            key_type: "cipher key",
        })?;
        Ok(buffer.into())
    }

    /// Internal method, returns the key as a [`chacha20::Key`]
    pub(crate) fn as_chacha_key(&self) -> &chacha20::Key {
        chacha20::Key::from_slice(&*self.key)
    }
}

impl TryFrom<&[u8]> for CipherKey {
    type Error = KeyError;

    fn try_from(value: &[u8]) -> Result<Self, Self::Error> {
        let key: &[u8; 32] = value.try_into().map_err(|_| KeyError::IncorrectLength {
            key_type: "Cipher Key",
            expected: 32,
            actual: value.len(),
        })?;
        Ok(CipherKey { key: (*key).into() })
    }
}

impl From<[u8; 32]> for CipherKey {
    fn from(key: [u8; 32]) -> Self {
        CipherKey { key: key.into() }
    }
}

impl AsRef<CipherKey> for CipherKey {
    fn as_ref(&self) -> &CipherKey {
        self
    }
}

/// A key used for authentication
#[derive(Zeroize, Debug, Clone)]
#[zeroize(drop)]
pub struct HmacKey {
    /// The key proper
    pub(crate) key: FullyRedacted<[u8; 32]>,
}

impl HmacKey {
    /// Returns the bytes of the key, as a slice
    pub fn key_bytes(&self) -> &[u8] {
        self.key.as_ref()
    }
    /// Returns the bytes of the key, as an array reference
    pub fn key_bytes_array(&self) -> &[u8; 32] {
        self.key.as_ref()
    }
    /// Generate a random [`HmacKey`]
    ///
    /// # Errors
    ///
    /// Will return an error if the random number generator faults during generation
    pub fn random() -> Result<HmacKey, KeyError> {
        let mut buffer = [0_u8; 32];
        rand(&mut buffer).context(GenerationSnafu {
            key_type: "hmac key",
        })?;
        Ok(buffer.into())
    }
}

impl TryFrom<&[u8]> for HmacKey {
    type Error = KeyError;

    fn try_from(value: &[u8]) -> Result<Self, Self::Error> {
        let key: &[u8; 32] = value.try_into().map_err(|_| KeyError::IncorrectLength {
            key_type: "HMAC Key",
            expected: 32,
            actual: value.len(),
        })?;
        Ok(HmacKey { key: (*key).into() })
    }
}

impl From<[u8; 32]> for HmacKey {
    fn from(key: [u8; 32]) -> Self {
        HmacKey { key: key.into() }
    }
}

impl AsRef<HmacKey> for HmacKey {
    fn as_ref(&self) -> &HmacKey {
        self
    }
}

/// A number used only once
///
/// # Encoding
///
/// The nonce is directly encoded as a string of 24 bytes, leading to the following wire format:
///
/// | Bytes   | Value    | Description                 |
/// |---------|----------|-----------------------------|
/// | `0..24` | contents | The contents of the `Nonce` |
#[derive(Zeroize, Clone, PartialEq, Eq)]
#[zeroize(drop)]
pub struct Nonce {
    /// The nonce proper
    pub(crate) nonce: FullyRedacted<[u8; 24]>,
}

impl std::fmt::Debug for Nonce {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        // Encode the nonce as hex
        let mut output = String::with_capacity(48);
        for byte in &*self.nonce {
            output.extend(format!("{:02x}", byte).chars());
        }
        f.debug_struct("Nonce").field("nonce", &output).finish()
    }
}

impl Nonce {
    /// Returns the bytes of the nonce, as a slice
    pub fn nonce_bytes(&self) -> &[u8] {
        self.nonce.as_ref()
    }
    /// Returns the bytes of the nonce, as an array reference
    pub fn nonce_bytes_array(&self) -> &[u8; 24] {
        self.nonce.as_ref()
    }
    /// Generate a random [`Nonce`]
    ///
    /// # Errors
    ///
    /// Will return an error if the random number generator faults during generation
    pub fn random() -> Result<Nonce, KeyError> {
        let mut buffer = [0_u8; 24];
        rand(&mut buffer).context(GenerationSnafu { key_type: "nonce" })?;
        Ok(buffer.into())
    }
    /// Returns an all zero nonce
    pub fn zero() -> Nonce {
        Self {
            nonce: [0_u8; 24].into(),
        }
    }
    /// Increments the none by one, treating it as a single, bytewise little endian number
    #[must_use]
    pub fn inc(&self) -> Nonce {
        let mut ret = self.clone();
        let mut carry = true;
        for byte in ret.nonce.iter_mut() {
            if carry {
                if *byte == u8::MAX {
                    *byte = 0;
                } else {
                    *byte += 1;
                    carry = false;
                }
            } else {
                break;
            }
        }
        ret
    }
    /// Internal method, returns a reference to the key as a [`chacha20::XNonce`]
    pub(crate) fn as_xnonce(&self) -> &chacha20::XNonce {
        chacha20::XNonce::from_slice(self.nonce.as_ref())
    }
}

impl TryFrom<&[u8]> for Nonce {
    type Error = KeyError;

    fn try_from(value: &[u8]) -> Result<Self, Self::Error> {
        let key: &[u8; 24] = value.try_into().map_err(|_| KeyError::IncorrectLength {
            key_type: "Nonce",
            expected: 32,
            actual: value.len(),
        })?;
        Ok(Nonce {
            nonce: (*key).into(),
        })
    }
}

impl From<[u8; 24]> for Nonce {
    fn from(nonce: [u8; 24]) -> Self {
        Nonce {
            nonce: nonce.into(),
        }
    }
}

impl Encode for Nonce {
    type Error = KeyError;

    fn length(&self) -> usize {
        // Constant 24 bytes
        24
    }

    fn encode<W>(&self, writer: &mut W) -> Result<(), Self::Error>
    where
        Self: Sized,
        W: std::io::Write,
    {
        // Directly encode the nonce
        self.nonce.encode(writer).context(NonceEncSnafu)
    }

    fn decode<R>(reader: &mut R) -> Result<Self, Self::Error>
    where
        Self: Sized,
        R: std::io::Read,
    {
        // Directly decode the nonce
        let nonce = FullyRedacted::decode(reader).context(NonceEncSnafu)?;
        Ok(Self { nonce })
    }
}

/// A combined [`CipherKey`] and [`HmacKey`]
#[derive(Zeroize, Debug, Clone)]
#[non_exhaustive]
pub struct Key {
    /// Sub-key used for encryption
    pub cipher: CipherKey,
    /// Sub-key used for authentication
    pub hmac: HmacKey,
}

impl Key {
    /// Create a new Key from known subkeys
    pub fn new(cipher: CipherKey, hmac: HmacKey) -> Self {
        Key { cipher, hmac }
    }
    /// Generate a new, random key
    ///
    /// # Errors
    ///
    /// Will return an error if the underlying RNG experiences a failure.
    pub fn random() -> Result<Self, KeyError> {
        let cipher = CipherKey::random()?;
        let hmac = HmacKey::random()?;
        Ok(Key { cipher, hmac })
    }
}

impl AsRef<CipherKey> for Key {
    fn as_ref(&self) -> &CipherKey {
        &self.cipher
    }
}

impl AsRef<HmacKey> for Key {
    fn as_ref(&self) -> &HmacKey {
        &self.hmac
    }
}

impl AsRef<Key> for Key {
    fn as_ref(&self) -> &Key {
        self
    }
}

/// A master key, from which normal use keys are derived
#[derive(Zeroize, Debug, Clone)]
#[non_exhaustive]
#[zeroize(drop)]
pub struct MasterKey {
    /// Normal, contained key, used for encryption and decryption operations with the master key
    pub key: Key,
    /// Random noise bytes used to generate derived keys
    pub noise: FullyRedacted<[u8; 128]>,
}

impl MasterKey {
    /// Create a new `MasterKey` from parts
    pub fn new(key: Key, noise: &[u8; 128]) -> Self {
        MasterKey {
            key,
            noise: (*noise).into(),
        }
    }
    /// Create a new, random `MasterKey`
    ///
    /// # Errors
    ///
    /// Will return an error if there is a fault in the RNG during generation.
    pub fn random() -> Result<Self, KeyError> {
        let key = Key::random()?;
        let mut bytes = [0_u8; 128];
        rand(&mut bytes).context(GenerationSnafu {
            key_type: "noise byte array",
        })?;
        Ok(Self {
            key,
            noise: bytes.into(),
        })
    }

    /// Derives a new `Key` from this `MasterKey`
    ///
    /// # Errors
    ///
    /// Will return an error if the RNG faults during derivation
    pub fn derive(&self) -> Result<(Key, [u8; 32]), KeyError> {
        let mut uuid = [0_u8; 32];
        rand(&mut uuid).context(GenerationSnafu {
            key_type: "Derived Key",
        })?;

        Ok((self.derive_again(uuid), uuid))
    }

    /// Rederives a `Key` given a UUID
    pub fn derive_again(&self, uuid: [u8; 32]) -> Key {
        let mut hasher = blake3::Hasher::new_derive_key(
            "Replicator Fri Nov 26 05:15:14 AM EST 2021 Key Derivation",
        );
        // Add in the uuid
        hasher.update(&uuid);
        // Add in our key material
        hasher.update(self.key.cipher.key_bytes());
        hasher.update(self.key.hmac.key_bytes());
        // Add in our noise
        hasher.update(self.noise.as_ref());
        // Generate our key
        let mut cipher = [0_u8; 32];
        let mut hmac = [0_u8; 32];
        let mut output = hasher.finalize_xof();
        output.fill(&mut cipher);
        output.fill(&mut hmac);

        Key::new(
            CipherKey { key: cipher.into() },
            HmacKey { key: hmac.into() },
        )
    }

    /// Derives an [`HmacKey`] from this [`MasterKey`] using the provided context string
    pub fn derive_hmac(&self, context: &str) -> HmacKey {
        let mut hasher = blake3::Hasher::new_derive_key(context);
        // Domain separation
        hasher.update("HMAC".as_bytes());
        // Invaild utf-8 sequence for good luck
        hasher.update(&[195_u8, 40_u8]);
        // Add in our key material
        hasher.update(self.key.cipher.key_bytes());
        hasher.update(self.key.hmac.key_bytes());
        // Add in our noise
        hasher.update(self.noise.as_ref());
        let key = FullyRedacted::from(*hasher.finalize().as_bytes());
        HmacKey { key }
    }

    /// Derives an [`CipherKey`] from this [`MasterKey`] using the provided context string
    pub fn derive_cipher(&self, context: &str) -> CipherKey {
        let mut hasher = blake3::Hasher::new_derive_key(context);
        // Domain separation
        hasher.update("cipher".as_bytes());
        // Invaild utf-8 sequence for good luck
        hasher.update(&[195_u8, 40_u8]);
        // Add in our key material
        hasher.update(self.key.cipher.key_bytes());
        hasher.update(self.key.hmac.key_bytes());
        // Add in our noise
        hasher.update(self.noise.as_ref());
        let key = FullyRedacted::from(*hasher.finalize().as_bytes());
        CipherKey { key }
    }
}

impl AsRef<CipherKey> for MasterKey {
    fn as_ref(&self) -> &CipherKey {
        self.key.as_ref()
    }
}

impl AsRef<HmacKey> for MasterKey {
    fn as_ref(&self) -> &HmacKey {
        self.key.as_ref()
    }
}

impl AsRef<Key> for MasterKey {
    fn as_ref(&self) -> &Key {
        &self.key
    }
}

#[cfg(test)]
mod tests {
    use super::*;
    mod smoke {
        use super::*;
        // Ensure generated values are non-zero
        #[test]
        fn non_zero() {
            let zeros_32 = [0_u8; 32];
            let zeros_128 = [0_u8; 128];
            let cipher = CipherKey::random().unwrap();
            assert_ne!(&zeros_32, cipher.key_bytes());
            let hmac = CipherKey::random().unwrap();
            assert_ne!(&zeros_32, hmac.key_bytes());
            let key = Key::random().unwrap();
            assert_ne!(&zeros_32, key.hmac.key_bytes());
            assert_ne!(&zeros_32, key.hmac.key_bytes());
            let master_key = MasterKey::random().unwrap();
            assert_ne!(&zeros_32, master_key.key.cipher.key_bytes());
            assert_ne!(&zeros_32, master_key.key.hmac.key_bytes());
            assert_ne!(&zeros_128, &*master_key.noise);
        }
        // Make sure we aren't generating duplicate keys
        #[test]
        fn no_duplicates() {
            let cipher_1 = CipherKey::random().unwrap();
            let cipher_2 = CipherKey::random().unwrap();
            assert_ne!(&*cipher_1.key, &*cipher_2.key);
            let hmac_1 = HmacKey::random().unwrap();
            let hmac_2 = HmacKey::random().unwrap();
            assert_ne!(&*hmac_1.key, &*hmac_2.key);
            let master_1 = MasterKey::random().unwrap();
            let master_2 = MasterKey::random().unwrap();
            assert_ne!(&*master_1.noise, &*master_2.noise);
        }
        // Make sure derived keys are non-zero
        #[test]
        fn non_zero_derive() {
            let master = MasterKey::random().unwrap();
            let (key, _uuid) = master.derive().unwrap();
            assert_ne!(key.cipher.key_bytes_array(), &[0_u8; 32]);
            assert_ne!(key.hmac.key_bytes_array(), &[0_u8; 32]);
        }
        // Make sure derived keys are non-equal
        #[test]
        fn non_equal_derive() {
            let master = MasterKey::random().unwrap();
            let (key_1, _uuid) = master.derive().unwrap();
            let (key_2, _uuid) = master.derive().unwrap();
            assert_ne!(key_1.cipher.key_bytes(), key_2.cipher.key_bytes());
            assert_ne!(key_1.hmac.key_bytes(), key_2.hmac.key_bytes());
        }
        // Make sure CipherKey's try_from method works correctly
        #[test]
        fn cipher_key_try_from() {
            let correct_length_key = [0_u8; 32];
            let incorrect_length_key = [0_u8; 33];
            let correct = CipherKey::try_from(&correct_length_key[..]);
            assert!(correct.is_ok());
            let incorrect = CipherKey::try_from(&incorrect_length_key[..]);
            assert!(matches!(incorrect, Err(KeyError::IncorrectLength { .. })));
        }
        // Make sure HmacKey's try_from method works correctly
        #[test]
        fn hmac_key_try_from() {
            let correct_length_key = [0_u8; 32];
            let incorrect_length_key = [0_u8; 33];
            let correct = HmacKey::try_from(&correct_length_key[..]);
            assert!(correct.is_ok());
            let incorrect = HmacKey::try_from(&incorrect_length_key[..]);
            assert!(matches!(incorrect, Err(KeyError::IncorrectLength { .. })));
        }
        // Make sure Nonce's try_from method works correctly
        #[test]
        fn nonce_try_from() {
            let correct_length_key = [0_u8; 24];
            let incorrect_length_key = [0_u8; 25];
            let correct = Nonce::try_from(&correct_length_key[..]);
            assert!(correct.is_ok());
            let incorrect = Nonce::try_from(&incorrect_length_key[..]);
            assert!(matches!(incorrect, Err(KeyError::IncorrectLength { .. })));
        }
        // Make sure master key deriving produces consistent values
        #[test]
        fn master_key_derive_consistent() {
            let master_key = MasterKey::new(
                Key::new(
                    CipherKey::try_from([1_u8; 32].as_ref()).unwrap(),
                    HmacKey::try_from([2_u8; 32].as_ref()).unwrap(),
                ),
                &[3_u8; 128],
            );
            let magic_string = "hello i am a salt";
            // Test hmac keys
            let hmac_key = master_key.derive_hmac(magic_string);
            let hmac_string = hex::encode(hmac_key.key_bytes());
            assert_eq!(
                hmac_string,
                "10c5c3da5fd62bf77849d97058a3dbd1d72e82c875ba3eb01baaaec1c7788b77"
            );
            // Test cipher keys
            let cipher_key = master_key.derive_cipher(magic_string);
            let cipher_string = hex::encode(cipher_key.key_bytes());
            assert_eq!(
                cipher_string,
                "d3afe6ff3934b328a53b4b8ea57980f88be8c156880e022caedbee1bb915a5b8"
            );
        }
        // Make sure nonce rollove works correctly
        #[test]
        fn nonce_rollover() {
            // First byte carry
            let mut nonce = Nonce::zero();
            // Increment it 257 times
            for _ in 0..257 {
                nonce = nonce.inc();
            }
            assert_eq!(
                nonce.nonce_bytes_array(),
                &[
                    0x01_u8, 0x01_u8, 0x00_u8, 0x00_u8, 0x00_u8, 0x00_u8, 0x00_u8, 0x00_u8,
                    0x00_u8, 0x00_u8, 0x00_u8, 0x00_u8, 0x00_u8, 0x00_u8, 0x00_u8, 0x00_u8,
                    0x00_u8, 0x00_u8, 0x00_u8, 0x00_u8, 0x00_u8, 0x00_u8, 0x00_u8, 0x00_u8
                ]
            );
            // Second byte carry
            let mut nonce = Nonce::zero();
            // Increment it 65537 times
            for _ in 0..65537 {
                nonce = nonce.inc();
            }
            assert_eq!(
                nonce.nonce_bytes_array(),
                &[
                    0x01_u8, 0x00_u8, 0x01_u8, 0x00_u8, 0x00_u8, 0x00_u8, 0x00_u8, 0x00_u8,
                    0x00_u8, 0x00_u8, 0x00_u8, 0x00_u8, 0x00_u8, 0x00_u8, 0x00_u8, 0x00_u8,
                    0x00_u8, 0x00_u8, 0x00_u8, 0x00_u8, 0x00_u8, 0x00_u8, 0x00_u8, 0x00_u8
                ]
            );
            // Third byte carry
            let mut nonce = Nonce::zero();
            // Increment it 65793 times
            for _ in 0..65793 {
                nonce = nonce.inc();
            }
            assert_eq!(
                nonce.nonce_bytes_array(),
                &[
                    0x01_u8, 0x01_u8, 0x01_u8, 0x00_u8, 0x00_u8, 0x00_u8, 0x00_u8, 0x00_u8,
                    0x00_u8, 0x00_u8, 0x00_u8, 0x00_u8, 0x00_u8, 0x00_u8, 0x00_u8, 0x00_u8,
                    0x00_u8, 0x00_u8, 0x00_u8, 0x00_u8, 0x00_u8, 0x00_u8, 0x00_u8, 0x00_u8
                ]
            );
        }
    }
    mod encode {
        use super::*;
        use std::io::Cursor;
        // Round trip a nonce
        #[test]
        fn nonce_round_trip() {
            // Make a nonce
            let nonce = Nonce::random().unwrap();
            // Encode it
            let mut buffer = Cursor::new(Vec::new());
            nonce.encode(&mut buffer).unwrap();
            // Verify the length
            let buffer = buffer.into_inner();
            assert_eq!(buffer.len(), nonce.length());
            let mut buffer = Cursor::new(buffer);
            // Decode it
            let output_nonce = Nonce::decode(&mut buffer).unwrap();
            assert_eq!(nonce, output_nonce);
        }
    }
}
