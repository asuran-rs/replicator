use balloon_hash::Balloon;
use redacted::FullyRedacted;
use snafu::{ensure, ResultExt};
use zeroize::Zeroizing;

use crate::{
    crypto::key::{
        CipherKey, GenerationSnafu, HmacKey, IOSnafu, Key, KeyError, MasterKey, Nonce,
        PasswordSnafu, VerificationSnafu,
    },
    file::traits::Encode,
    rand::rand,
};
/// A [`MasterKey`] that has been encrypted with a user provided password
///
/// Balloon Hashing is used to hash the user provided password, producing the user provided output.
/// This output is then run through Blake3 in KDF mode, with the following context strings, to
/// produce the payload:
///
/// - `replicator 2022-05-23T19:48-04:00 cipher`: [`CipherKey`]
/// - `replicator 2022-05-23T19:48-04:00 hmac`: [`HmacKey`]
/// - `replicator 2022-05-23T19:48-04:00 nonce`: [`Nonce`]
///
/// Due the the authors lazyiness, despite the nonce only being 24 bytes, Blake3 in kdf mode is
/// still used on the default settings, providing a 32 byte output, of which only the first 24 bytes
/// are used.
///
/// # Encoding Format
///
/// The encoding format for the `EncryptedMasterKey` is as follows:
///
/// | Bytes      | Value   | Description                                   |
/// |------------|---------|-----------------------------------------------|
/// | `0..192`   | payload | Encrypted, combined master key material       |
/// | `192..224` | salt    | Balloon Hashing salt, used for key generation |
/// | `224..256` | hmac    | HMAC of the payload ++ salt                   |
///
#[derive(Debug)]
pub struct PasswordEncryptedMasterKey {
    /// Bytes containing the encrypted key
    pub payload: FullyRedacted<[u8; 192]>,
    /// The salt used for the password
    pub salt: FullyRedacted<[u8; 32]>,
    /// The HMAC of `payload ++ salt`
    pub hmac: FullyRedacted<[u8; 32]>,
}

impl PasswordEncryptedMasterKey {
    /// Calculates the HMAC value
    fn calcuate_hmac(payload: &[u8; 192], salt: &[u8; 32], hmac_key: &HmacKey) -> [u8; 32] {
        let mut hasher = blake3::Hasher::new_keyed(hmac_key.key_bytes_array());
        hasher.update(payload);
        hasher.update(salt);
        let output = hasher.finalize();
        *output.as_bytes()
    }
    /// Generates a cipher key, an hmac key, and a nonce from the password and salt
    fn calcuate_crypto(
        password: &[u8],
        salt: &[u8; 32],
    ) -> Result<(CipherKey, HmacKey, Nonce), KeyError> {
        // Get the raw output from the balloon, this will only be 32 bytes so we will have to expand
        // it
        let balloon = Balloon::<blake3::Hasher>::default();
        let raw_output = Zeroizing::new(
            balloon
                .hash(password, salt)
                .context(PasswordSnafu)?
                .to_vec(),
        );
        // Expand the output into three different keys using blake3
        let cipher = CipherKey::try_from(blake3::derive_key(
            "replicator 2022-05-23T19:48-04:00 cipher",
            &raw_output[..],
        ))
        .expect("Impossible length mismatch");
        let hmac = HmacKey::try_from(blake3::derive_key(
            "replicator 2022-05-23T19:48-04:00 hmac",
            &raw_output[..],
        ))
        .expect("Impossible length mismatch");
        let nonce = Nonce::try_from(
            &blake3::derive_key("replicator 2022-05-23T19:48-04:00 nonce", &raw_output[..])[..24],
        )
        .expect("Impossible length mismatch");
        Ok((cipher, hmac, nonce))
    }

    /// Encrypts a [`MasterKey`] with the provided password
    ///
    /// # Errors
    ///   * If the rng fails while generating salt
    ///   * If there is an error while performing cryptography
    pub fn encrypt(key: &MasterKey, password: &[u8]) -> Result<Self, KeyError> {
        use chacha20::{
            cipher::{KeyIvInit, StreamCipher},
            XChaCha20,
        };
        // pack everything into the payload
        let mut payload: FullyRedacted<[u8; 192]> = FullyRedacted::from([0_u8; 192]);
        payload[..32].copy_from_slice(key.key.cipher.key_bytes());
        payload[32..64].copy_from_slice(key.key.hmac.key_bytes());
        payload[64..].copy_from_slice(&*key.noise);
        // Generate a salt
        let mut salt: FullyRedacted<[u8; 32]> = FullyRedacted::from([0_u8; 32]);
        rand(&mut salt[..]).context(GenerationSnafu {
            key_type: "password salt",
        })?;
        // Generate the key material
        let (cipher, hmac, nonce) = Self::calcuate_crypto(password, salt.as_ref())?;
        // Build the cipher
        let mut chacha = XChaCha20::new(cipher.as_chacha_key(), nonce.as_xnonce());
        // Encrypt the payload
        chacha.apply_keystream(&mut payload[..]);
        // Calculate the hmac
        let hmac = FullyRedacted::from(Self::calcuate_hmac(payload.as_ref(), salt.as_ref(), &hmac));
        Ok(Self {
            payload,
            salt,
            hmac,
        })
    }

    /// Decrypts a [`MasterKey`] with the provided password
    ///
    /// # Errors
    ///   * If an error occurs during cryptograhpy operations
    ///   * If the HMAC fails verification, such as if the password is wrong
    pub fn decrypt(&self, password: &[u8]) -> Result<MasterKey, KeyError> {
        use chacha20::{
            cipher::{KeyIvInit, StreamCipher},
            XChaCha20,
        };
        // Generate the key material
        let (cipher, hmac, nonce) = Self::calcuate_crypto(password, self.salt.as_ref())?;
        // Verify the HMAC
        let mut hasher = blake3::Hasher::new_keyed(hmac.key_bytes_array());
        hasher.update(&*self.payload);
        hasher.update(&*self.salt);
        let hash = hasher.finalize();
        let tag: [u8; 32] = *self.hmac.as_ref();
        ensure!(hash == tag, VerificationSnafu);
        // Build the cipher
        let mut chacha = XChaCha20::new(cipher.as_chacha_key(), nonce.as_xnonce());
        // Decrypt the payload
        let mut payload = self.payload;
        chacha.apply_keystream(&mut *payload);
        let mut cipher_key = CipherKey {
            key: FullyRedacted::from([0_u8; 32]),
        };
        cipher_key.key.copy_from_slice(&payload[0..32]);
        let mut hmac_key = HmacKey {
            key: FullyRedacted::from([0_u8; 32]),
        };
        hmac_key.key.copy_from_slice(&payload[32..64]);
        let mut noise: FullyRedacted<[u8; 128]> = FullyRedacted::from([0_u8; 128]);
        noise.copy_from_slice(&payload[64..]);
        let key = Key::new(cipher_key, hmac_key);
        let master_key = MasterKey::new(key, noise.as_ref());

        Ok(master_key)
    }
}

impl Encode for PasswordEncryptedMasterKey {
    type Error = KeyError;

    fn length(&self) -> usize {
        // Payload (192) + salt (32) + hmac (32) = 256 bytes
        256
    }

    fn encode<W>(&self, writer: &mut W) -> Result<(), Self::Error>
    where
        Self: Sized,
        W: std::io::Write,
    {
        // Write the payload
        writer.write_all(self.payload.as_ref()).context(IOSnafu)?;
        // Write the salt
        writer.write_all(self.salt.as_ref()).context(IOSnafu)?;
        // Write the HMAC
        writer.write_all(self.hmac.as_ref()).context(IOSnafu)?;
        Ok(())
    }

    fn decode<R>(reader: &mut R) -> Result<Self, Self::Error>
    where
        Self: Sized,
        R: std::io::Read,
    {
        let mut payload = [0_u8; 192];
        let mut salt = [0_u8; 32];
        let mut hmac = [0_u8; 32];
        // Read the feilds
        reader.read_exact(&mut payload).context(IOSnafu)?;
        reader.read_exact(&mut salt).context(IOSnafu)?;
        reader.read_exact(&mut hmac).context(IOSnafu)?;
        Ok(Self {
            payload: payload.into(),
            salt: salt.into(),
            hmac: hmac.into(),
        })
    }
}

#[cfg(test)]
mod tests {
    use super::*;
    mod smoke {
        use super::*;
        // Make sure encryption actually encrypts keys
        #[test]
        fn encryption_encrypts() {
            let zeros_32 = [0_u8; 32];
            let key = MasterKey::random().unwrap();
            let encrypted =
                PasswordEncryptedMasterKey::encrypt(&key, "password".as_bytes()).unwrap();
            assert_ne!(&encrypted.payload[0..32], key.key.cipher.key_bytes());
            assert_ne!(&encrypted.payload[32..64], key.key.hmac.key_bytes());
            assert_ne!(&encrypted.payload[64..], &*key.noise);
            assert_ne!(&*encrypted.salt, &zeros_32);
            assert_ne!(&*encrypted.hmac, &zeros_32);
        }
        // Key encryption round trip
        #[test]
        fn encrypt_decrypt() {
            let key = MasterKey::random().unwrap();
            let encrypted =
                PasswordEncryptedMasterKey::encrypt(&key, "password".as_bytes()).unwrap();
            let decrypted = encrypted.decrypt("password".as_bytes()).unwrap();
            assert_eq!(key.key.cipher.key_bytes(), decrypted.key.cipher.key_bytes());
            assert_eq!(key.key.hmac.key_bytes(), decrypted.key.hmac.key_bytes());
            assert_eq!(&*key.noise, &*decrypted.noise);
        }
        // Damage an encrypted key and ensure it doesn't decode
        #[test]
        fn reject_bad_key() {
            let key = MasterKey::random().unwrap();
            let mut encrypted =
                PasswordEncryptedMasterKey::encrypt(&key, "password".as_bytes()).unwrap();
            let bytes: &mut [u8] = encrypted.payload.as_mut();
            bytes[0] = bytes[0].overflowing_add(1_u8).0;
            let decrypted = encrypted.decrypt("password".as_bytes());
            assert!(decrypted.is_err());
        }
    }
    mod encode {
        use super::*;
        use std::io::Cursor;
        // Make sure the round trip for the header works properly
        #[test]
        fn enc_master_key_round_trip() {
            let master_key = MasterKey::random().unwrap();
            let enc =
                PasswordEncryptedMasterKey::encrypt(&master_key, "password".as_bytes()).unwrap();
            // Make a buffer
            let mut buffer = Cursor::new(Vec::<u8>::new());
            // Encode the key
            enc.encode(&mut buffer).unwrap();
            // Verify the buffer's length
            let buffer = buffer.into_inner();
            assert_eq!(enc.length(), 256);
            assert_eq!(buffer.len(), enc.length());
            // Put the buffer back and attempt to decode it
            let mut buffer = Cursor::new(buffer);
            let enc_2 = PasswordEncryptedMasterKey::decode(&mut buffer).unwrap();
            // Decrypt it
            let master_key_2 = enc_2.decrypt("password".as_bytes()).unwrap();
            // Verify equality
            assert_eq!(
                master_key.key.cipher.key_bytes(),
                master_key_2.key.cipher.key_bytes()
            );
            assert_eq!(
                master_key.key.hmac.key_bytes(),
                master_key_2.key.hmac.key_bytes()
            );
            assert_eq!(&*master_key.noise, &*master_key_2.noise);
        }
    }
}
