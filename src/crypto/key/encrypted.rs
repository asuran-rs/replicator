/// Internal module for the password encrypted version of a key
mod password;

pub use password::PasswordEncryptedMasterKey;
