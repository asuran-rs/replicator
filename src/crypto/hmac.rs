use std::{
    fmt::{Debug, Display},
    hash::Hash,
};

use redacted::FullyRedacted;
use snafu::{ResultExt, Snafu};
use subtle::ConstantTimeEq;

use crate::{crypto::key::HmacKey, file::traits::Encode};

/// Error encoding or decoding an HMAC tag
#[derive(Debug, Snafu)]
#[non_exhaustive]
pub struct HmacEncodeError {
    /// Underlying error
    source: crate::error::SliceError,
}

/// Wrapper type for HMAC tags
///
/// Implements constant time equality
///
/// # Encoding
///
/// An `HmacTag` is encoded as a string of 32 bytes, resulting in the following wire format:
///
/// | Bytes   | Value | Description   |
/// |---------|-------|---------------|
/// | `0..32` | `tag` | The `HmacTag` |
#[derive(Clone, Copy)]
pub struct HmacTag {
    /// The bytes composing the tag
    pub tag: FullyRedacted<[u8; 32]>,
}

impl Hash for HmacTag {
    fn hash<H: std::hash::Hasher>(&self, state: &mut H) {
        self.tag.hash(state);
    }
}

impl HmacTag {
    /// Generates a new `HmacTag` from a byte array
    pub fn new(array: [u8; 32]) -> Self {
        Self { tag: array.into() }
    }
    /// Generates a new, all zeros `HmacTag`
    pub fn zero() -> Self {
        Self {
            tag: [0_u8; 32].into(),
        }
    }
}

impl From<[u8; 32]> for HmacTag {
    fn from(item: [u8; 32]) -> Self {
        Self::new(item)
    }
}

impl From<&[u8; 32]> for HmacTag {
    fn from(item: &[u8; 32]) -> Self {
        Self::new(*item)
    }
}

impl PartialEq for HmacTag {
    fn eq(&self, other: &Self) -> bool {
        self.tag.ct_eq(other.tag.as_ref()).into()
    }
}

impl AsRef<[u8]> for HmacTag {
    fn as_ref(&self) -> &[u8] {
        self.tag.as_ref()
    }
}

impl Eq for HmacTag {}

impl Encode for HmacTag {
    type Error = HmacEncodeError;

    fn length(&self) -> usize {
        // Constant length of 32 bytes
        32
    }

    fn encode<W>(&self, writer: &mut W) -> Result<(), Self::Error>
    where
        Self: Sized,
        W: std::io::Write,
    {
        self.tag.encode(writer).context(HmacEncodeSnafu)
    }

    fn decode<R>(reader: &mut R) -> Result<Self, Self::Error>
    where
        Self: Sized,
        R: std::io::Read,
    {
        let tag = FullyRedacted::decode(reader).context(HmacEncodeSnafu)?;
        Ok(Self { tag })
    }
}

impl PartialOrd for HmacTag {
    fn partial_cmp(&self, other: &Self) -> Option<std::cmp::Ordering> {
        // TODO: Maybe make this constant time?
        self.tag.partial_cmp(&other.tag)
    }
}

impl Ord for HmacTag {
    fn cmp(&self, other: &Self) -> std::cmp::Ordering {
        self.tag.cmp(&other.tag)
    }
}

impl Debug for HmacTag {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        let mut hex = "0x".to_string();
        for byte in self.tag.iter() {
            let string = format!("{:X?}", byte);
            hex.push_str(&string);
        }
        f.debug_struct("HmacTag").field("tag", &hex).finish()
    }
}

impl Display for HmacTag {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        let mut hex = "0x".to_string();
        for byte in self.tag.iter() {
            let string = format!("{:X?}", byte);
            hex.push_str(&string);
        }
        write!(f, "{}", hex)
    }
}

/// Wrapper type for namespaced HMAC tags
///
/// # Encoding
///
/// A `NamespacedTag` is encoded as the underlying `HmacTag`, followed by the [`String`] in its
/// default encoding (see [`Encode`] trait documentation for details), resulting in the following
/// wire format:
///
/// | Bytes               | Value    | Description                          |
/// |---------------------|----------|--------------------------------------|
/// | `0..32`             | `tag`    | The [`HmacTag`] proper               |
/// | `32..40`            | `length` | The length of the namespace string   |
/// | `40..(length + 40)` | contents | The contents of the namespace string |
///
#[derive(Debug, Clone, PartialEq, Eq)]
pub struct NamespacedTag {
    /// The tag proper
    pub tag: HmacTag,
    /// The namespace of the tag
    pub namespace: String,
}

impl Encode for NamespacedTag {
    type Error = HmacEncodeError;

    fn length(&self) -> usize {
        // Add the tag and the namespace together
        self.tag.length() + self.namespace.length()
    }

    fn encode<W>(&self, writer: &mut W) -> Result<(), Self::Error>
    where
        Self: Sized,
        W: std::io::Write,
    {
        // Write the tag
        self.tag.encode(writer)?;
        // Write the namespace
        self.namespace.encode(writer).context(HmacEncodeSnafu)?;
        Ok(())
    }

    fn decode<R>(reader: &mut R) -> Result<Self, Self::Error>
    where
        Self: Sized,
        R: std::io::Read,
    {
        // Decode the tag
        let tag = HmacTag::decode(reader)?;
        // Decode the namespace
        let namespace = String::decode(reader).context(HmacEncodeSnafu)?;
        Ok(Self { tag, namespace })
    }
}

/// Utility type for performing an HMAC
#[derive(Debug, Clone)]
pub struct Hmac<'a> {
    /// Key used for performing the hmac
    key: &'a HmacKey,
}

impl<'a> Hmac<'a> {
    /// Create a new `Hmac` with the given key
    pub fn new<K>(key: &'a K) -> Self
    where
        K: AsRef<HmacKey>,
    {
        let key = key.as_ref();
        Self { key }
    }

    /// Generate an [`HmacTag`] from a byte buffer
    pub fn tag(&self, bytes: impl AsRef<[u8]>) -> HmacTag {
        let bytes = bytes.as_ref();
        let tag_bytes: [u8; 32] = blake3::keyed_hash(self.key.key_bytes_array(), bytes).into();
        HmacTag {
            tag: tag_bytes.into(),
        }
    }

    /// Generate a [`NamespacedTag`], with domain separation provided by the given namespace
    pub fn namespaced_tag(
        &self,
        namespace: impl AsRef<str>,
        bytes: impl AsRef<[u8]>,
    ) -> NamespacedTag {
        let bytes = bytes.as_ref();
        let namespace = namespace.as_ref();
        let mut hasher = blake3::Hasher::new_keyed(self.key.key_bytes_array());
        // Add in the namespace
        hasher.update(namespace.as_bytes());
        // Add in an invalid utf-8 sequence
        // This acts as part of the namespace, and ensures that no namespace can be a prefix of
        // another, as Rust's strs can only contain valid utf-8 sequences.
        hasher.update(&[195_u8, 40_u8]);
        // Finally hash the contents
        hasher.update(bytes);
        // Generate the tag
        let tag_bytes: [u8; 32] = hasher.finalize().into();
        let tag = HmacTag {
            tag: tag_bytes.into(),
        };
        NamespacedTag {
            tag,
            namespace: namespace.to_string(),
        }
    }
}

#[cfg(test)]
mod tests {
    use super::*;
    use crate::crypto::HmacKey;
    use std::io::Cursor;
    mod smoke {
        use super::*;
        // Tags should be non equal
        #[test]
        fn non_equal() {
            let key = HmacKey::random().unwrap();
            let hmac = Hmac::new(&key);
            // Regular tags should be non-equal
            let thing_1 = [0_u8; 256];
            let thing_2 = [1_u8; 256];
            let hash_1 = hmac.tag(&thing_1);
            let hash_2 = hmac.tag(&thing_2);
            assert_ne!(hash_1, hash_2);
            // Namespaced tags should be non-equal
            let name_1 = hmac.namespaced_tag("name", &thing_1);
            let name_2 = hmac.namespaced_tag("name", &thing_2);
            assert_ne!(name_1, name_2);
        }
        // Repeated calls should be equal
        #[test]
        fn equal() {
            let key = HmacKey::random().unwrap();
            let hmac = Hmac::new(&key);
            let thing = [0_u8; 256];
            // Regular tags should be equal
            let hash_1 = hmac.tag(&thing);
            let hash_2 = hmac.tag(&thing);
            assert_eq!(hash_1, hash_2);
            // Namespaced tags should be equal
            let hash_1 = hmac.namespaced_tag("name", &thing);
            let hash_2 = hmac.namespaced_tag("name", &thing);
            assert_eq!(hash_1, hash_2);
        }
        // The same thing in different namespaces should be non-equal
        #[test]
        fn namespace_non_equal() {
            let key = HmacKey::random().unwrap();
            let hmac = Hmac::new(&key);
            let thing = [0_u8; 256];
            let hash_1 = hmac.namespaced_tag("first", &thing);
            let hash_2 = hmac.namespaced_tag("second", &thing);
            assert_ne!(hash_1, hash_2);
        }
    }
    mod encode {
        use super::*;
        // round trip an hmac tag
        #[test]
        fn hmac_tag() {
            // Generate some data and the key/hmac
            let key = HmacKey::random().unwrap();
            let mut data = [0_u8; 128];
            crate::rand::rand(&mut data).unwrap();
            let hmac = Hmac::new(&key);
            // Generate the tag
            let tag = hmac.tag(data);
            // Encode it
            let mut buffer = Cursor::new(Vec::new());
            tag.encode(&mut buffer).unwrap();
            // Very the length
            let buffer = buffer.into_inner();
            assert_eq!(buffer.len(), tag.length());
            let mut buffer = Cursor::new(buffer);
            let output_tag = HmacTag::decode(&mut buffer).unwrap();
            assert_eq!(tag, output_tag);
        }
        // round trip a namespaced tag
        #[test]
        fn namespaced_tag() {
            // Generate some data and the key/hmac
            let key = HmacKey::random().unwrap();
            let mut data = [0_u8; 128];
            crate::rand::rand(&mut data).unwrap();
            let hmac = Hmac::new(&key);
            // Generate the tag
            let tag = hmac.namespaced_tag("testing-namespace", &data);
            // Encode it
            let mut buffer = Cursor::new(Vec::new());
            tag.encode(&mut buffer).unwrap();
            // Verify the length
            let buffer = buffer.into_inner();
            assert_eq!(buffer.len(), tag.length());
            let mut buffer = Cursor::new(buffer);
            // Decode it
            let output_tag = NamespacedTag::decode(&mut buffer).unwrap();
            assert_eq!(tag, output_tag);
        }
    }
}
