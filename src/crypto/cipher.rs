use snafu::{OptionExt, Snafu};

use crate::crypto::{CipherKey, Nonce};

/// Error type for cipher operations
#[derive(Debug, Snafu)]
#[non_exhaustive]
pub enum CipherError {
    /// The end of the keystream was reached
    EndOfKeystream,
    /// Byte sequence was bigger than entire KeyStream (>4GiB)
    SequenceTooLong,
}

/// The state for a cipher
pub struct Cipher<'a> {
    /// Key being used for encryption
    key: &'a CipherKey,
    /// Nonce being used for encryption
    nonce: Nonce,
    /// Current offset in the keystream
    ///
    /// This is currently kept as a `u32` to strongly enforce no more than 4GiB being written with
    /// each key+nonce pair.
    position: u32,
}

impl<'a> Cipher<'a> {
    /// Create a new `Cipher` from a given key and nonce.
    ///
    /// This consumes the nonce to discourage reuse. The position will start at 0.
    pub fn new<K>(key: &'a K, nonce: Nonce) -> Self
    where
        K: AsRef<CipherKey>,
    {
        let key = key.as_ref();
        Self {
            key,
            nonce,
            position: 0,
        }
    }
    /// Applies a key stream to a byte buffer
    ///
    /// Will return the new poistion of the `Cipher`, measured after applying the keystream.
    ///
    /// # Errors
    ///
    /// Will return an error if:
    ///   * The end of the keystream is encountered
    ///   * Some underlying cryptograhpy error occurs
    ///
    ///   In the event that the end of the keystream would be reached in the process of applying the
    ///   keystream, this method will perform no operations before erroring.
    pub fn apply_keystream(&mut self, mut buffer: impl AsMut<[u8]>) -> Result<u32, CipherError> {
        use chacha20::{
            cipher::{KeyIvInit, StreamCipher, StreamCipherSeek},
            XChaCha20,
        };

        let buffer: &mut [u8] = buffer.as_mut();
        // Make sure that we aren't going to overflow the keystream
        let length: u32 = buffer
            .len()
            .try_into()
            .map_err(|_| CipherError::SequenceTooLong)?;
        let new_pos = self
            .position
            .checked_add(length)
            .context(EndOfKeystreamSnafu)?;
        // Start up the cipher
        let mut chacha = XChaCha20::new(self.key.as_chacha_key(), self.nonce.as_xnonce());
        chacha
            .try_seek(self.position)
            .map_err(|_| CipherError::EndOfKeystream)?;
        chacha
            .try_apply_keystream(buffer)
            .map_err(|_| CipherError::EndOfKeystream)?;
        let current_pos: u32 = chacha.try_current_pos().expect("Invalid cipher state");
        debug_assert_eq!(current_pos, new_pos);
        self.position = current_pos;
        Ok(current_pos)
    }
    /// Returns the current position of the `Cipher`
    pub fn position(&self) -> u32 {
        self.position
    }
    /// Sets the position of the `Cipher`
    pub fn seek(&mut self, to: u32) {
        self.position = to;
    }
}

#[cfg(test)]
mod tests {
    use super::*;
    use crate::crypto::key::{CipherKey, Nonce};
    mod smoke {
        use super::*;
        // Applying a keystream should change the bytes
        #[test]
        fn changes_bytes() {
            let key = CipherKey::random().unwrap();
            let nonce = Nonce::zero();
            let mut zeros = [0_u8; 1024];
            let zeros_unchaging = [0_u8; 1024];
            let mut cipher = Cipher::new(&key, nonce);
            cipher.apply_keystream(&mut zeros).unwrap();
            assert_ne!(zeros, zeros_unchaging);
        }
        // Round trip encryption-decryption
        #[test]
        fn round_trip() {
            let key = CipherKey::random().unwrap();
            let nonce = Nonce::zero();
            let mut zeros = [0_u8; 1024];
            let zeros_unchaging = [0_u8; 1024];
            let mut cipher = Cipher::new(&key, nonce.clone());
            cipher.apply_keystream(&mut zeros).unwrap();
            let mut cipher = Cipher::new(&key, nonce);
            cipher.apply_keystream(&mut zeros).unwrap();
            assert_eq!(zeros, zeros_unchaging);
        }
        // Test offset
        #[test]
        fn offset() {
            let key = CipherKey::random().unwrap();
            let nonce = Nonce::zero();
            let mut zeros = [0_u8; 1024];
            let mut cipher = Cipher::new(&key, nonce);
            assert_eq!(cipher.position(), 0);
            cipher.apply_keystream(&mut zeros).unwrap();
            assert_eq!(cipher.position(), 1024);
        }
        // Different keys should have different results
        #[test]
        fn different_keys() {
            let key1 = CipherKey::random().unwrap();
            let key2 = CipherKey::random().unwrap();
            let mut zeros1 = [0_u8; 1024];
            let mut zeros2 = [0_u8; 1024];
            let mut cipher1 = Cipher::new(&key1, Nonce::zero());
            cipher1.apply_keystream(&mut zeros1).unwrap();
            let mut cipher2 = Cipher::new(&key2, Nonce::zero());
            cipher2.apply_keystream(&mut zeros2).unwrap();
            assert_ne!(zeros1, zeros2);
        }
        // Different nonces should have different results
        #[test]
        fn different_nonces() {
            let key = CipherKey::random().unwrap();
            let nonce1 = Nonce::random().unwrap();
            let nonce2 = nonce1.inc();
            let mut zeros1 = [0_u8; 1024];
            let mut zeros2 = [0_u8; 1024];
            let mut cipher1 = Cipher::new(&key, nonce1);
            cipher1.apply_keystream(&mut zeros1).unwrap();
            let mut cipher2 = Cipher::new(&key, nonce2);
            cipher2.apply_keystream(&mut zeros2).unwrap();
            assert_ne!(zeros1, zeros2);
        }
        // Different start points should have different results
        #[test]
        fn diffrent_seeks() {
            let key = CipherKey::random().unwrap();
            let nonce = Nonce::random().unwrap();
            let mut zeros1 = [0_u8; 1024];
            let mut zeros2 = [0_u8; 1024];
            let mut cipher1 = Cipher::new(&key, nonce.clone());
            cipher1.seek(1024);
            cipher1.apply_keystream(&mut zeros1).unwrap();
            let mut cipher2 = Cipher::new(&key, nonce);
            cipher2.apply_keystream(&mut zeros2).unwrap();
            assert_ne!(zeros1, zeros2);
        }
    }
}
