use std::collections::BTreeMap;

use crate::crypto::Nonce;

/// Handler for multiple nonces in a file
///
/// Keeps track of where nonces start and end, and also calculates to compensate for the fact that
/// renonocing starts a new key stream at offset 0
#[derive(Debug, Clone)]
pub struct NonceHandler {
    /// Internal map of nonce starts to nonce
    pub(crate) map: BTreeMap<u64, Nonce>,
}

impl NonceHandler {
    /// Creates a new `NonceHandler` using the provided [`Nonce`] as the first in the file
    pub fn new(initial_nonce: Nonce) -> Self {
        let mut map = BTreeMap::new();
        map.insert(0, initial_nonce);
        Self { map }
    }
    /// Inserts a new [`Nonce`] into the map
    ///
    /// The `position` argument specifies the offset of the first byte in the file to use this nonce
    ///
    /// It is considered partially invalid to replace an existing nonce, there is no fundamental
    /// rule preventing it, but as this represents an extreme smell, a `debug_assert` is provided
    /// when the `paranoid_asserts` feature is set.
    pub fn insert(&mut self, position: u64, nonce: Nonce) {
        let x = self.map.insert(position, nonce);
        #[cfg(feature = "paranoid-asserts")]
        debug_assert!(x.is_none());
    }
    /// Determines the nonce and keystream position of a given byte in the file
    pub fn lookup(&self, position: u64) -> (Nonce, u64) {
        let mut iterator = self.map.iter();
        // Start off assuming we use the first nonce
        if let Some((mut start, _)) = iterator.next() {
            // Loop through the map
            //
            // While `position` is strictly before the start of the nonce, up date the nonce in use
            for (s, _) in iterator {
                if s > &position {
                    break;
                }
                start = s;
            }
            // Start should be less than or equal to position
            let start = *start;
            debug_assert!(start <= position);
            // Look up the nonce
            let nonce = self
                .map
                .get(&start)
                .expect("Somehow missing a key we had a few lines ago")
                .clone();
            // Calculate the position in this nonce's key stream
            let ret_pos = position - start;
            (nonce, ret_pos)
        } else {
            unreachable!("NonceHandler will always have at least one nonce");
        }
    }
}

#[cfg(test)]
mod tests {
    use super::*;
    mod smoke {
        use super::*;
        // Utility function
        fn test(
            handler: &NonceHandler,
            position: u64,
            expected_nonce: &Nonce,
            expected_offset: u64,
        ) {
            let (nonce, offset) = handler.lookup(position);
            assert_eq!(expected_nonce, &nonce);
            assert_eq!(expected_offset, offset);
        }
        // Basic check, make sure we get back out the same nonce
        #[test]
        fn basic() {
            let nonce = Nonce::random().unwrap();
            let nonce_handler = NonceHandler::new(nonce.clone());
            assert_eq!(
                nonce.nonce_bytes(),
                nonce_handler.lookup(1234).0.nonce_bytes()
            );
        }
        // Check to make sure that mulitple nonces work properly
        #[test]
        fn muliple() {
            // Get some nonces
            let nonce_0 = Nonce::random().unwrap();
            let nonce_100 = Nonce::random().unwrap();
            let nonce_200 = Nonce::random().unwrap();
            let nonce_300 = Nonce::random().unwrap();
            // setup the handler
            let mut handler = NonceHandler::new(nonce_0.clone());
            handler.insert(100, nonce_100.clone());
            handler.insert(200, nonce_200.clone());
            handler.insert(300, nonce_300.clone());
            // Verify some indexes
            test(&handler, 0, &nonce_0, 0);
            test(&handler, 10, &nonce_0, 10);
            test(&handler, 50, &nonce_0, 50);
            test(&handler, 100, &nonce_100, 0);
            test(&handler, 110, &nonce_100, 10);
            test(&handler, 150, &nonce_100, 50);
            test(&handler, 200, &nonce_200, 0);
            test(&handler, 210, &nonce_200, 10);
            test(&handler, 250, &nonce_200, 50);
            test(&handler, 300, &nonce_300, 0);
            test(&handler, 310, &nonce_300, 10);
            test(&handler, 350, &nonce_300, 50);
        }
    }
}
