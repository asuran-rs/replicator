use std::{borrow::Cow, io::Cursor};

use byteorder::{ReadBytesExt, WriteBytesExt};
use snafu::{ResultExt, Snafu};
use tracing::{instrument, log::trace};

use crate::{
    crypto::{Hmac, HmacKey, HmacTag, Nonce},
    file::traits::Encode,
};

/// Errors in encoding or decoding an [`Action`]
#[derive(Debug, Snafu)]
#[non_exhaustive]
pub enum ActionError {
    /// Error while encoding the discriminant
    #[snafu(display("Error while encoding the discriminant: {}", variant))]
    EncodeDiscriminant {
        /// The underlying error
        source: std::io::Error,
        /// The variant
        variant: &'static str,
    },
    /// Error while encoding or decoding a nonce
    #[snafu(display("Error while encoding/decoding nonce. encoding: {}", encoding))]
    NonceEnc {
        /// The underlying error
        source: crate::error::KeyError,
        /// Encoding or decoding
        encoding: bool,
    },
    /// Error while encoding or decoding a tag
    #[snafu(display("Error while encoding/decoding tag. encoding: {}", encoding))]
    TagEnc {
        /// The underlying error
        source: crate::error::HmacEncodeError,
        /// Encoding or decoding
        encoding: bool,
    },
    /// Error while encoding or decoding a length
    #[snafu(display("Error while encoding/decoding length. encoding: {}", encoding))]
    LengthEnc {
        /// The underlying error
        source: std::io::Error,
        /// Encoding or decoding
        encoding: bool,
    },
    /// Error while encoding or decoding a slice
    #[snafu(display("Error while encoding/decoding length. encoding: {}", encoding))]
    SliceEnc {
        /// The underlying error
        source: std::io::Error,
        /// Encoding or decoding
        encoding: bool,
    },
    /// Error while encoding or decoding a node
    #[snafu(display("Error while encoding/decoding node. encoding: {}", encoding))]
    NodeEnc {
        /// The underlying error
        source: crate::error::HmacEncodeError,
        /// Encoding or decoding
        encoding: bool,
    },
    /// Error while encoding or decoding a root
    #[snafu(display("Error while encoding/decoding root. encoding: {}", encoding))]
    RootEnc {
        /// The underlying error
        source: crate::error::HmacEncodeError,
        /// Encoding or decoding
        encoding: bool,
    },
    /// Error while decoding discriminant
    DecodeDiscriminant {
        /// The underlying error
        source: std::io::Error,
    },
    /// Invalid discriminant
    #[snafu(display("Invalid action type discriminant: {}", discriminant))]
    InvalidDiscriminant {
        /// The invalid discriminant
        discriminant: u8,
    },
    /// Byte segment larger than maximum size
    #[snafu(display(
        "Byte segment longer than maximum size of {} bytes. length: {}",
        u32::MAX,
        length
    ))]
    BytesTooLong {
        /// The lenght of the offending chunk
        length: usize,
    },
}

/// Offset between the start of an [`Action`] containing a [`ActionType::Bytes`] and its payload
pub const BYTES_PAYLOAD_OFFSET: u64 = 37;

/// Generic representation of control actions that can be encoded in an encrypted file
///
/// # Encoding
///
/// An `Action` is encoded as its hmac, followed by the encoding of the contained `ActionType`, with
/// the following wire format:
///
/// | Bytes   | Value  | Description                  |
/// |---------|--------|------------------------------|
/// | `0..32` | `tag`  | The HMAC of the action       |
/// | `32..N` | action | The contained [`ActionType`] |
#[derive(Debug, PartialEq, Eq, Clone)]
#[non_exhaustive]
pub struct Action<'a> {
    /// The hmac of the encoded `Action`
    pub tag: HmacTag,
    /// The type of action being taken
    pub action: ActionType<'a>,
}

impl<'a> Action<'a> {
    /// Creates a new action, producing the tag from the provided key
    ///
    /// # Errors
    ///
    /// Will return an error if the action fails to encode
    pub fn new<T>(action: ActionType<'a>, key: &T) -> Result<Self, ActionError>
    where
        T: AsRef<HmacKey>,
    {
        let hmac = Hmac::new(key);
        // Encode the action to generate the tag
        let mut buffer = Cursor::new(Vec::new());
        action.encode(&mut buffer)?;
        // Generate the tag
        let buffer = buffer.into_inner();
        let tag = hmac.tag(&buffer);
        Ok(Self { tag, action })
    }

    /// Verifies that an action matches its HMAC tag
    ///
    /// # Errors
    ///
    /// Will return an error if the action fails to encode
    pub fn verify<T>(&self, key: &T) -> Result<bool, ActionError>
    where
        T: AsRef<HmacKey>,
    {
        let hmac = Hmac::new(key);
        // Encode the action to generate the tag
        let mut buffer = Cursor::new(Vec::new());
        self.action.encode(&mut buffer)?;
        // Generate the tag
        let buffer = buffer.into_inner();
        let tag = hmac.tag(&buffer);
        // Compare for equality
        Ok(tag == self.tag)
    }
}

impl<'a> Encode for Action<'a> {
    type Error = ActionError;

    fn length(&self) -> usize {
        // Simply add the lengths
        self.tag.length() + self.action.length()
    }

    fn encode<W>(&self, writer: &mut W) -> Result<(), Self::Error>
    where
        Self: Sized,
        W: std::io::Write,
    {
        // encode the tag
        self.tag
            .encode(writer)
            .context(TagEncSnafu { encoding: true })?;
        // encode the action
        self.action.encode(writer)
    }

    fn decode<R>(reader: &mut R) -> Result<Self, Self::Error>
    where
        Self: Sized,
        R: std::io::Read,
    {
        // Decode the tag
        let tag = HmacTag::decode(reader).context(TagEncSnafu { encoding: false })?;
        // Decode the action
        let action = ActionType::decode(reader)?;
        Ok(Self { tag, action })
    }
}

/// The types of [`Action`] that can be encoded
///
/// # Encoding
///
/// This enum is encoded with a single byte discriminator, followed by the variant specific
/// encoding. See the documentation of each variant for details
#[derive(PartialEq, Eq, Clone)]
pub enum ActionType<'a> {
    /// Sets a new nonce for the encrypted file
    ///
    /// # Encoding
    ///
    /// This variant has discriminant `0x00`, and has the following wire format:
    ///
    /// | Bytes   | Value   | Description                 |
    /// |---------|---------|-----------------------------|
    /// | `0`     | `0x00`  | Discriminator               |
    /// | `1..25` | `nonce` | The [`Nonce`] being encoded |
    SetNonce {
        /// The Nonce to be encoded
        nonce: Nonce,
    },
    /// Encodes a chunk of encrypted bytes in the file
    ///
    /// # Encoding
    ///
    /// This variant has discriminant `0x01` and the following wire format:
    ///
    /// | Bytes    | Value     | Description                  |
    /// |----------|-----------|------------------------------|
    /// | `0`      | `0x01`    | Discriminant                 |
    /// | `1..5`   | `length`  | Number of bytes encoded      |
    /// | `5..n+5` | `payload` | The chunk of encrypted bytes |
    Bytes {
        /// The payload
        payload: Cow<'a, [u8]>,
    },
    /// Encodes a node in the merkel tree
    ///
    /// This will contain the [`HmacTag`]'s of two other [`Action`]'s, and this [`Action`]'s serves
    /// as the combined [`HmacTag`].
    ///
    /// # Encoding
    ///
    /// This variant has discriminant `0x02` and the following wire format:
    ///
    /// | Bytes    | Value   | Description                    |
    /// |----------|---------|--------------------------------|
    /// | `0`      | `0x02`  | Discriminant                   |
    /// | `1..33`  | `left`  | The left child of this `Node`  |
    /// | `33..65` | `right` | The right child of this `Node` |
    Node {
        /// The left child of this node
        left: HmacTag,
        /// The right child of this node
        right: HmacTag,
    },
    /// Creates a new root of the merkel tree
    ///
    /// This [`Action`] contains the [`HmacTag`] of the [`ActionType::Node`]
    /// that will become the new root, as well as the [`HmacTag`] of the
    /// previous root node
    ///
    /// # Encoding
    ///
    /// This variant has discriminant `0x03` and the following wire format:
    ///
    /// | Bytes    | Value         | Description                                          |
    /// |----------|---------------|------------------------------------------------------|
    /// | `0`      | `0x03`        | Discriminant                                         |
    /// | `1..33`  | new_root      | The [`HmacTag`] of the new root [`ActionType::Node`] |
    /// | `33..65` | previous_root | The [`HmacTag`] of the previous `ActionType::Root`   |
    Root {
        /// The new root [`ActionType::Node`]
        new_root: HmacTag,
        /// The previous [`ActionType::Root`]
        previous_root: HmacTag,
    },
}

impl<'a> std::fmt::Debug for ActionType<'a> {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        match self {
            Self::SetNonce { nonce } => f.debug_struct("SetNonce").field("nonce", nonce).finish(),
            Self::Bytes { payload } => f
                .debug_struct("Bytes")
                .field("length", &payload.len())
                .finish(),
            Self::Node { left, right } => f
                .debug_struct("Node")
                .field("left", left)
                .field("right", right)
                .finish(),
            Self::Root {
                new_root,
                previous_root,
            } => f
                .debug_struct("Root")
                .field("new_root", new_root)
                .field("previous_root", previous_root)
                .finish(),
        }
    }
}

impl<'a> ActionType<'a> {
    /// Returns the discriminant for this variant
    pub fn discriminant(&self) -> u8 {
        match self {
            ActionType::SetNonce { .. } => 0x00_u8,
            ActionType::Bytes { .. } => 0x01_u8,
            ActionType::Node { .. } => 0x02_u8,
            ActionType::Root { .. } => 0x03_u8,
        }
    }
    /// Returns the variants name
    fn name(&self) -> &'static str {
        match self {
            ActionType::SetNonce { .. } => "SetNonce",
            ActionType::Bytes { .. } => "Bytes",
            ActionType::Node { .. } => "Node",
            ActionType::Root { .. } => "Root",
        }
    }
}

impl<'a> Encode for ActionType<'a> {
    type Error = ActionError;

    fn length(&self) -> usize {
        // Contained values plus 1 for the discriminator
        let len = match self {
            // Only contains the nonce, so use its length
            ActionType::SetNonce { nonce } => nonce.length(),
            // The length of the payload, plus 4 for the length
            ActionType::Bytes { payload } => payload.len() + 4,
            // The length of each of the tags
            ActionType::Node { left, right } => left.length() + right.length(),
            // The length of each of the tags
            ActionType::Root {
                new_root,
                previous_root,
            } => new_root.length() + previous_root.length(),
        };
        len + 1
    }

    #[instrument(skip(writer))]
    fn encode<W>(&self, writer: &mut W) -> Result<(), Self::Error>
    where
        Self: Sized,
        W: std::io::Write,
    {
        trace!("Encoding the discriminant");
        let disc = self.discriminant();
        writer.write_u8(disc).context(EncodeDiscriminantSnafu {
            variant: self.name(),
        })?;
        trace!("Applying variant specific encoding");
        match self {
            ActionType::SetNonce { nonce } => {
                trace!("Encoding nonce");
                nonce
                    .encode(writer)
                    .context(NonceEncSnafu { encoding: true })
            }
            ActionType::Bytes { payload } => {
                // Check to make sure that the length fits in the allowed maximum
                let length: u32 = payload.len().try_into().map_err(|_| {
                    BytesTooLongSnafu {
                        length: payload.len(),
                    }
                    .build()
                })?;
                trace!("Encoding length");
                length
                    .encode(writer)
                    .context(LengthEncSnafu { encoding: true })?;
                trace!("Encoding the payload");
                writer
                    .write_all(payload)
                    .context(SliceEncSnafu { encoding: true })
            }
            ActionType::Node { left, right } => {
                trace!("Encoding the left child");
                left.encode(writer)
                    .context(NodeEncSnafu { encoding: true })?;
                trace!("Encoding the right child");
                right
                    .encode(writer)
                    .context(NodeEncSnafu { encoding: true })
            }
            ActionType::Root {
                new_root,
                previous_root,
            } => {
                trace!("Encoding the new root");
                new_root
                    .encode(writer)
                    .context(RootEncSnafu { encoding: true })?;
                trace!("Encoding the previous root");
                previous_root
                    .encode(writer)
                    .context(RootEncSnafu { encoding: true })
            }
        }
    }

    #[instrument(skip(reader), err)]
    fn decode<R>(reader: &mut R) -> Result<Self, Self::Error>
    where
        Self: Sized,
        R: std::io::Read,
    {
        // Decode the discriminant
        let disc = reader.read_u8().context(DecodeDiscriminantSnafu)?;
        // Match and apply variant specific decoding
        match disc {
            0x00_u8 => {
                // Set Nonce - Directly decode the nonce
                let nonce = Nonce::decode(reader).context(NonceEncSnafu { encoding: false })?;
                Ok(ActionType::SetNonce { nonce })
            }
            0x01_u8 => {
                // Bytes - Decode the length and payload
                let length = u32::decode(reader).context(LengthEncSnafu { encoding: false })?;
                let mut payload = vec![0_u8; length as usize];
                reader
                    .read_exact(&mut payload)
                    .context(SliceEncSnafu { encoding: false })?;
                Ok(ActionType::Bytes {
                    payload: Cow::from(payload),
                })
            }
            0x02_u8 => {
                // Decode the left child
                let left = HmacTag::decode(reader).context(NodeEncSnafu { encoding: false })?;
                // Decode the right child
                let right = HmacTag::decode(reader).context(NodeEncSnafu { encoding: false })?;
                Ok(ActionType::Node { left, right })
            }
            0x03_u8 => {
                // Decode the new root's tag
                let new_root = HmacTag::decode(reader).context(RootEncSnafu { encoding: false })?;
                // Decode the previous root's tag
                let previous_root =
                    HmacTag::decode(reader).context(RootEncSnafu { encoding: false })?;
                Ok(ActionType::Root {
                    new_root,
                    previous_root,
                })
            }
            x => InvalidDiscriminantSnafu { discriminant: x }.fail(),
        }
    }
}

#[cfg(test)]
mod tests {
    use super::*;
    use crate::crypto::Key;
    use std::io::Cursor;
    mod encode_action_type {
        use super::*;
        // Round trip utility function
        fn round_trip(item: &ActionType<'_>) {
            // Encode it
            let mut buffer = Cursor::new(Vec::new());
            item.encode(&mut buffer).unwrap();
            // Verify the length
            let buffer = buffer.into_inner();
            assert_eq!(buffer.len(), item.length());
            // Decode it
            let mut buffer = Cursor::new(buffer);
            let output = ActionType::decode(&mut buffer).unwrap();
            assert_eq!(item, &output);
        }
        // Round trip a SetNonce
        #[test]
        fn set_nonce() {
            let nonce = Nonce::random().unwrap();
            let action = ActionType::SetNonce { nonce };
            round_trip(&action);
        }
        // Round trip a Bytes
        #[test]
        fn bytes() {
            let payload = vec![0_u8; 1024];
            let action = ActionType::Bytes {
                payload: Cow::from(payload),
            };
            round_trip(&action);
        }
        // Round trip a Node
        #[test]
        fn node() {
            let bytes_left = [0_u8; 32];
            let bytes_right = [1_u8; 32];
            let key = Key::random().unwrap();
            let hmac = Hmac::new(&key);
            let left = hmac.tag(&bytes_left);
            let right = hmac.tag(&bytes_right);
            let node = ActionType::Node { left, right };
            round_trip(&node);
        }
        // Round trip a root
        #[test]
        fn root() {
            let bytes_new_root = [0_u8; 32];
            let bytes_previous_root = [1_u8; 32];
            let key = Key::random().unwrap();
            let hmac = Hmac::new(&key);
            let new_root = hmac.tag(&bytes_new_root);
            let previous_root = hmac.tag(&bytes_previous_root);
            let node = ActionType::Root {
                new_root,
                previous_root,
            };
            round_trip(&node);
        }
    }

    mod encode_action_struct {
        use super::*;
        // Round trip utility function
        fn round_trip<'a>(item: &Action<'a>) -> Action<'a> {
            // Encode it
            let mut buffer = Cursor::new(Vec::new());
            item.encode(&mut buffer).unwrap();
            // Verify the length
            let buffer = buffer.into_inner();
            assert_eq!(buffer.len(), item.length());
            // Decode it
            let mut buffer = Cursor::new(buffer);
            let output = Action::decode(&mut buffer).unwrap();
            assert_eq!(item, &output);
            output
        }
        // Round trip a SetNonce
        #[test]
        fn set_nonce() {
            let key = Key::random().unwrap();
            let action_type = ActionType::SetNonce {
                nonce: Nonce::random().unwrap(),
            };
            let action = Action::new(action_type, &key).unwrap();
            let output = round_trip(&action);
            assert!(output.verify(&key).unwrap());
        }
        // Round trip a Bytes
        #[test]
        fn bytes() {
            let key = Key::random().unwrap();
            let payload = vec![0_u8; 1024];
            let action_type = ActionType::Bytes {
                payload: Cow::from(payload),
            };
            let action = Action::new(action_type, &key).unwrap();
            let output = round_trip(&action);
            assert!(output.verify(&key).unwrap());
        }
        // Round trip a Node
        #[test]
        fn node() {
            let bytes_left = [0_u8; 32];
            let bytes_right = [1_u8; 32];
            let key = Key::random().unwrap();
            let hmac = Hmac::new(&key);
            let left = hmac.tag(&bytes_left);
            let right = hmac.tag(&bytes_right);
            let node = ActionType::Node { left, right };
            let action = Action::new(node, &key).unwrap();
            let output = round_trip(&action);
            assert!(output.verify(&key).unwrap());
        }
        // Rount trip a Root
        #[test]
        fn root() {
            let bytes_new_root = [0_u8; 32];
            let bytes_previous_root = [1_u8; 32];
            let key = Key::random().unwrap();
            let hmac = Hmac::new(&key);
            let new_root = hmac.tag(&bytes_new_root);
            let previous_root = hmac.tag(&bytes_previous_root);
            let root = ActionType::Root {
                new_root,
                previous_root,
            };
            let action = Action::new(root, &key).unwrap();
            let output = round_trip(&action);
            assert!(output.verify(&key).unwrap());
        }
    }
}
