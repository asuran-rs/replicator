//! Type for representing the file's header

use byteorder::{ReadBytesExt, WriteBytesExt};
use snafu::{ResultExt, Snafu};

use crate::{
    crypto::{Nonce, PasswordEncryptedMasterKey},
    file::traits::Encode,
};

/// Errors in decoding a header
#[derive(Debug, Snafu)]
#[non_exhaustive]
pub enum HeaderError {
    /// An I/O Error occurred while reading/writing the header
    IO {
        /// The underlying error
        source: std::io::Error,
    },
    /// An error occurred while encoding/decoding the encrypted master key1
    MasterKey {
        /// The underlying error
        source: crate::error::KeyError,
    },
    /// An invalid discriminant was encountered while decoding
    InvalidDiscriminant {
        /// The invalid value
        value: u8,
    },
}

/// Header for an encrypted file
///
/// # Encoding Format
///
/// The Header starts with a single discriminant byte, which can take the following values, and
/// indicate the corresponding variants:
///
/// | Discriminant | Variant              | Description                                       |
/// |--------------|----------------------|---------------------------------------------------|
/// | `0x00`       | `Header::StoredKey`  | File contains an encrypted master key             |
/// | `0x01`       | `Header::DerivedKey` | File contains an encrypted uuid for a derived key |
///
/// ## Stored Key
///
/// The [`Header::StoredKey`] variant contains an [`PasswordEncryptedMasterKey`] as the only entry
/// in the header, its format is as follows:
///
/// | Bytes      | Value                          | Description                                       |
/// |------------|--------------------------------|---------------------------------------------------|
/// | `0`        | `0x00`                         | Discriminant byte                                 |
/// | `1..257`   | [`PasswordEncryptedMasterKey`] | The encrypted master key, see its docs for format |
/// | `257..265` | `chunk_size`                   | The chunk size for this file                      |
///
/// ## Derived Key
///
/// The [`Header::DerivedKey`] variant contains an, encrypted, key derivation uuid, the nonce used
/// to encrypt the key derivation uuid, and an hmac (generated using the master key) of the combined
/// uuid + nonce, its format is as follows:
///
/// | Bytes    | Value            | Description                         |
/// |----------|------------------|-------------------------------------|
/// | `0`      | `0x01`           | Discriminant byte                   |
/// | `1..33`  | `encrypted_uuid` | Encrypted uuid for a derived key    |
/// | `33..57` | `nonce`          | Nonce used to encrypt uuid          |
/// | `57..89` | `hmac`           | hmac of `encrypted_uuid` ++ `nonce` |
/// | `89..97` | `chunk_size`                   | The chunk size for this file                      |
#[derive(Debug)]
pub enum Header {
    /// Indicates a file contains its master key
    ///
    /// This is type `0_u8`
    StoredKey {
        /// The [`PasswordEncryptedMasterKey`]
        encrypted_key: PasswordEncryptedMasterKey,
        /// The chunk size, in bytes
        chunk_size: u64,
    },
    /// Indicates that a file is encrypted with a derived key
    ///
    /// This is type `1_u8`
    DerivedKey {
        /// The (encrypted) `uuid` required to regenerate the key
        encrypted_uuid: [u8; 32],
        /// Nonce used to encrypt the uuid
        nonce: Nonce,
        /// A hmac, produced using the master key, of this instances `encrypted_uuid`
        hmac: [u8; 32],
        /// The chunk size, in bytes
        chunk_size: u64,
    },
}

impl Header {
    /// Gets the discriminator for this variant of header
    pub fn discriminiator(&self) -> u8 {
        match self {
            Header::StoredKey { .. } => 0,
            Header::DerivedKey { .. } => 1,
        }
    }
    /// Gets the chunk size
    pub fn chunk_size(&self) -> u64 {
        match self {
            Header::StoredKey { chunk_size, .. } | Header::DerivedKey { chunk_size, .. } => {
                *chunk_size
            }
        }
    }
}

impl Encode for Header {
    type Error = HeaderError;

    fn length(&self) -> usize {
        match self {
            Header::StoredKey { encrypted_key, .. } => {
                // The size of an encrypted_key, plus 1 for the discriminator, plus 8 for the chunk
                // size
                encrypted_key.length() + 1 + 8
            }
            Header::DerivedKey { .. } => {
                // - 1 byte for discriminant
                // - 32 bytes for uuid
                // - 24 bytes for nonce
                // - 32 bytes for hmac
                // - 8 bytes for the chunk size
                97
            }
        }
    }

    fn encode<W>(&self, mut writer: &mut W) -> Result<(), Self::Error>
    where
        Self: Sized,
        W: std::io::Write,
    {
        // Write the discriminator
        writer.write_u8(self.discriminiator()).context(IOSnafu)?;
        // Branch and write the body
        match self {
            Header::StoredKey {
                encrypted_key,
                chunk_size,
            } => {
                // Encode the master key
                encrypted_key.encode(&mut writer).context(MasterKeySnafu)?;
                // Encode the chunk size
                writer
                    .write_all(&chunk_size.to_le_bytes())
                    .context(IOSnafu)?;
            }
            Header::DerivedKey {
                encrypted_uuid,
                nonce,
                hmac,
                chunk_size,
            } => {
                // Encode the encrypted_uuid
                writer.write_all(encrypted_uuid).context(IOSnafu)?;
                // Encode the nonce
                writer.write_all(nonce.nonce_bytes()).context(IOSnafu)?;
                // Encode the hmac
                writer.write_all(hmac).context(IOSnafu)?;
                // Encode the chunk_size
                writer
                    .write_all(&chunk_size.to_le_bytes())
                    .context(IOSnafu)?;
            }
        }
        Ok(())
    }

    fn decode<R>(mut reader: &mut R) -> Result<Self, Self::Error>
    where
        Self: Sized,
        R: std::io::Read,
    {
        // Read the discriminator
        let discriminant: u8 = reader.read_u8().context(IOSnafu)?;
        match discriminant {
            0 => {
                // Decode master key
                let encrypted_key =
                    PasswordEncryptedMasterKey::decode(&mut reader).context(MasterKeySnafu)?;
                // Decode the chunk size
                let mut chunk_size_bytes = [0_u8; 8];
                reader
                    .read_exact(&mut chunk_size_bytes[..])
                    .context(IOSnafu)?;
                let chunk_size = u64::from_le_bytes(chunk_size_bytes);

                Ok(Self::StoredKey {
                    encrypted_key,
                    chunk_size,
                })
            }
            1 => {
                // Decode the uuid
                let mut uuid = [0_u8; 32];
                reader.read_exact(&mut uuid).context(IOSnafu)?;
                let mut nonce = [0_u8; 24];
                reader.read_exact(&mut nonce).context(IOSnafu)?;
                // Decode the hmac
                let mut hmac = [0_u8; 32];
                reader.read_exact(&mut hmac).context(IOSnafu)?;
                // Decode the chunk size
                let mut chunk_size_bytes = [0_u8; 8];
                reader
                    .read_exact(&mut chunk_size_bytes[..])
                    .context(IOSnafu)?;
                let chunk_size = u64::from_le_bytes(chunk_size_bytes);
                Ok(Self::DerivedKey {
                    encrypted_uuid: uuid,
                    nonce: nonce.into(),
                    hmac,
                    chunk_size,
                })
            }
            x => Err(HeaderError::InvalidDiscriminant { value: x }),
        }
    }
}

#[cfg(test)]
mod tests {
    use super::*;
    mod encode {
        use std::io::Cursor;

        use crate::crypto::{Cipher, Hmac, HmacTag};

        use super::*;
        // Round trip a StoredKey
        #[test]
        fn stored_key_round_trip() {
            let master_key = crate::crypto::MasterKey::random().unwrap();
            let enc_key =
                PasswordEncryptedMasterKey::encrypt(&master_key, "password".as_bytes()).unwrap();
            let header = Header::StoredKey {
                encrypted_key: enc_key,
                // 16 kiB
                chunk_size: 16 * 1024,
            };
            // Make a buffer
            let mut buffer = Cursor::new(Vec::<u8>::new());
            // Encode the header
            header.encode(&mut buffer).unwrap();
            // verify the length
            let buffer = buffer.into_inner();
            assert_eq!(buffer.len(), header.length());
            let mut buffer = Cursor::new(buffer);
            // Now attempt to decode
            let header_2 = Header::decode(&mut buffer).unwrap();
            if let Header::StoredKey {
                encrypted_key,
                chunk_size,
            } = header_2
            {
                // Attempt to decrypt the key
                let dec_key = encrypted_key.decrypt("password".as_bytes()).unwrap();
                // Verify equality
                assert_eq!(
                    dec_key.key.cipher.key_bytes(),
                    master_key.key.cipher.key_bytes()
                );
                assert_eq!(
                    dec_key.key.hmac.key_bytes(),
                    master_key.key.hmac.key_bytes()
                );
                assert_eq!(&*dec_key.noise, &*master_key.noise);
                assert_eq!(chunk_size, 16 * 1024);
            } else {
                panic!("wrong variant!")
            }
        }
        // Round trip a DerivedKey
        #[test]
        fn derived_key_round_trip() {
            let master_key = crate::crypto::MasterKey::random().unwrap();
            let (orig_key, orig_uuid) = master_key.derive().unwrap();
            // generate a nonce
            let mut nonce = [0_u8; 24];
            crate::rand::rand(&mut nonce).unwrap();
            // Encrypt the uuid
            let mut uuid = orig_uuid;
            let mut cipher = Cipher::new(&master_key, nonce.into());
            cipher.apply_keystream(&mut uuid).unwrap();
            // HMAC the uuid
            let hmac = Hmac::new(&master_key);
            let mut combined = uuid.to_vec();
            combined.extend(&nonce);
            let hmac_tag = hmac.tag(&combined);
            // Generate the header
            let orig_header = Header::DerivedKey {
                encrypted_uuid: uuid,
                nonce: nonce.into(),
                hmac: *hmac_tag.tag.as_ref(),
                // 16 kiB
                chunk_size: 16 * 1024,
            };
            // Write it to a buffer
            let mut buffer = Cursor::new(Vec::<u8>::new());
            orig_header.encode(&mut buffer).unwrap();
            // Verify the length
            let buffer = buffer.into_inner();
            assert_eq!(buffer.len(), orig_header.length());
            // Put it back in the buffer
            let mut buffer = Cursor::new(buffer);
            // Read back the header
            let new_header = Header::decode(&mut buffer).unwrap();
            // Make sure its the right variant
            if let Header::DerivedKey {
                encrypted_uuid,
                nonce,
                hmac,
                chunk_size,
            } = new_header
            {
                // Verify the HMAC
                let mut combined = encrypted_uuid.to_vec();
                combined.extend(nonce.nonce_bytes());
                let hmacer = Hmac::new(&master_key);
                let hmac_tag = hmacer.tag(&combined);
                assert_eq!(hmac_tag, HmacTag::new(hmac));
                // Decrypt the uuid
                let mut uuid = encrypted_uuid;
                let mut cipher = Cipher::new(&master_key, nonce);
                cipher.apply_keystream(&mut uuid).unwrap();
                // Rederive the key
                let new_key = master_key.derive_again(uuid);
                // Verify equality
                assert_eq!(new_key.cipher.key_bytes(), orig_key.cipher.key_bytes());
                assert_eq!(new_key.hmac.key_bytes(), orig_key.hmac.key_bytes());
                assert_eq!(chunk_size, 16 * 1024);
            } else {
                panic!("d wrong variant");
            }
        }
    }
}
