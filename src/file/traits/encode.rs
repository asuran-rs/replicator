use std::{
    io::{Read, Write},
    string::FromUtf8Error,
};

use byteorder::{LittleEndian, ReadBytesExt, WriteBytesExt};
use redacted::FullyRedacted;
use snafu::{ResultExt, Snafu};

/// Trait for things that have a fixed length binary encoding
///
/// # Encodings
///
/// Default implementations are provided for [`Vec<u8>`], [`String`], and [`FullyRedacted<[u8; N]>`], with
/// the following wire encodings
///
/// ## [`Vec<u8>`] and [`String`]
///
/// Both [`Vec<u8>`] and [`String`] are encoded as a little endian encoded [`u64`] (8 bytes),
/// followed by the specified number of bytes, for the following encoding:
///
/// | Bytes             | Value                    | Description                           |
/// |-------------------|--------------------------|---------------------------------------|
/// | `0..8`            | `length` (little endian) | Number of bytes in the `Vec`/`String` |
/// | `8..(length + 8)` | contents                 | The contents of the `Vec`/`String`    |
///
/// [`String`]'s will be validated to ensure valid utf-8 on decode, and will return an error if
/// validation fails
///
/// ## [`FullyRedacted<[u8; N]>`]
///
/// [`FullyRedacted<[u8; N]`>]'s get encoded as just their contained bytes, as their length is specified
/// by the compile time constant, giving them the following encoding:
///
/// | Bytes  | Value    | Description                            |
/// |--------|----------|----------------------------------------|
/// | `0..N` | contents | The contents of the `FullyRedacted<[u8; N]>` |
pub trait Encode {
    /// The error type for decoding
    type Error: std::error::Error + std::fmt::Debug;
    /// The number of bytes this value will take to encode
    fn length(&self) -> usize;
    /// Encode a value to a writer
    ///
    /// # Errors
    ///
    /// Should return an error if an underlying I/O error occurs
    fn encode<W>(&self, writer: &mut W) -> Result<(), Self::Error>
    where
        Self: Sized,
        W: Write;
    /// Decodes a value from a reader
    ///
    /// # Errors
    ///   * If the data is structurally invalid for this type
    ///   * If an underling I/O error occurs
    fn decode<R>(reader: &mut R) -> Result<Self, Self::Error>
    where
        Self: Sized,
        R: Read;
}

/// Error while encoding/decoding a slice
#[derive(Debug, Snafu)]
#[non_exhaustive]
pub enum SliceError {
    /// An I/O error occurred encoding a slice
    IOEncode {
        /// The underlying error
        source: std::io::Error,
    },
    /// An I/O error occurred decoding a slice
    IODecode {
        /// The underlying error
        source: std::io::Error,
    },
    /// An I/O error occurred encoding a string
    StringEncode {
        /// The underlying error
        source: std::io::Error,
    },
    /// An I/O error occurred decoding a string
    StringDecode {
        /// The underlying error
        source: std::io::Error,
    },
    /// Slice length too large to fit into memory
    SliceTooLong {
        /// The length of the bad slice
        length: u64,
    },
    /// String contained invalid utf-8
    InvalidUtf8 {
        /// The underlying error
        source: FromUtf8Error,
    },
}

impl Encode for Vec<u8> {
    type Error = SliceError;

    fn length(&self) -> usize {
        // Length of the vector, plus a u64 for length
        std::mem::size_of::<u64>() + self.len()
    }

    fn encode<W>(&self, writer: &mut W) -> Result<(), Self::Error>
    where
        Self: Sized,
        W: Write,
    {
        // first, encode the length
        let length: u64 = self.len() as u64;
        writer
            .write_u64::<LittleEndian>(length)
            .context(IOEncodeSnafu)?;
        // Then, encode the vector
        writer.write_all(self).context(IOEncodeSnafu)?;
        Ok(())
    }

    fn decode<R>(reader: &mut R) -> Result<Self, Self::Error>
    where
        Self: Sized,
        R: Read,
    {
        // first, read the length
        let length: u64 = reader.read_u64::<LittleEndian>().context(IODecodeSnafu)?;
        // Convert to usize
        let len: usize = length
            .try_into()
            .map_err(|_| SliceTooLongSnafu { length }.build())?;
        // Make a buffer
        let mut buffer = vec![0_u8; len];
        // Read the slice
        reader.read_exact(&mut buffer).context(IODecodeSnafu)?;
        // Return the buffer
        Ok(buffer)
    }
}

impl Encode for String {
    type Error = SliceError;

    fn length(&self) -> usize {
        // A u64 for the length plus the length (in bytes) of the string
        std::mem::size_of::<u64>() + self.as_bytes().len()
    }

    fn encode<W>(&self, writer: &mut W) -> Result<(), Self::Error>
    where
        Self: Sized,
        W: Write,
    {
        // First write the length
        let length = self.len() as u64;
        writer
            .write_u64::<LittleEndian>(length)
            .context(StringEncodeSnafu)?;
        // Then write the bytes
        writer
            .write_all(self.as_bytes())
            .context(StringEncodeSnafu)?;
        Ok(())
    }

    fn decode<R>(reader: &mut R) -> Result<Self, Self::Error>
    where
        Self: Sized,
        R: Read,
    {
        // First read the length
        let length: u64 = reader
            .read_u64::<LittleEndian>()
            .context(StringDecodeSnafu)?;
        // Make sure it will fit in memory
        let len: usize = length
            .try_into()
            .map_err(|_| SliceTooLongSnafu { length }.build())?;
        // Make a buffer
        let mut buffer = vec![0_u8; len];
        reader.read_exact(&mut buffer).context(StringDecodeSnafu)?;
        // Convert to a string
        String::from_utf8(buffer).context(InvalidUtf8Snafu)
    }
}

impl Encode for u32 {
    type Error = std::io::Error;

    fn length(&self) -> usize {
        // A u32 is always 4 bytes
        4
    }

    fn encode<W>(&self, writer: &mut W) -> Result<(), Self::Error>
    where
        Self: Sized,
        W: Write,
    {
        // Write the u32
        writer.write_u32::<LittleEndian>(*self)
    }

    fn decode<R>(reader: &mut R) -> Result<Self, Self::Error>
    where
        Self: Sized,
        R: Read,
    {
        // Read the u32
        reader.read_u32::<LittleEndian>()
    }
}

impl<const N: usize> Encode for FullyRedacted<[u8; N]> {
    type Error = SliceError;

    fn length(&self) -> usize {
        N
    }

    fn encode<W>(&self, writer: &mut W) -> Result<(), Self::Error>
    where
        Self: Sized,
        W: Write,
    {
        // Directly encode the bytes
        writer.write_all(self.as_ref()).context(IOEncodeSnafu)?;
        Ok(())
    }

    fn decode<R>(reader: &mut R) -> Result<Self, Self::Error>
    where
        Self: Sized,
        R: Read,
    {
        // Directly decode the bytes
        let mut buffer = FullyRedacted::from([0_u8; N]);
        reader.read_exact(buffer.as_mut()).context(IODecodeSnafu)?;
        Ok(buffer)
    }
}

#[cfg(test)]
mod tests {
    use super::*;
    use proptest::prelude::*;
    use std::io::{Cursor, Seek, SeekFrom};
    mod vec_encode {
        use super::*;
        proptest! {
            // In memory round trip test of vector encoding/decoding
            #[test]
            fn round_trip(input: Vec<u8>) {
                let mut buffer = Cursor::new(Vec::new());
                input.encode(&mut buffer).unwrap();
                let buffer = buffer.into_inner();
                assert_eq!(buffer.len(), input.length());
                let mut buffer = Cursor::new(buffer);
                let output = Vec::<u8>::decode(&mut buffer).unwrap();
                assert_eq!(input, output);
            }
        }
    }
    mod string_encode {
        use super::*;
        proptest! {
            // In memory round trip test of string encoding/decoding
            #[test]
            fn round_trip(input: String) {
                let mut buffer = Cursor::new(Vec::new());
                input.encode(&mut buffer).unwrap();
                // Validate the length
                let buffer = buffer.into_inner();
                assert_eq!(buffer.len(), input.length());
                let mut buffer = Cursor::new(buffer);
                let output = String::decode(&mut buffer).unwrap();
                assert_eq!(input, output);
            }
        }
    }
    mod redacted_encode {
        use super::*;
        // In memory round trip testing
        #[test]
        fn round_trip() {
            use crate::rand::rand;
            // Get some buffers
            let mut buffer_32 = FullyRedacted::from([0_u8; 32]);
            let mut buffer_64 = FullyRedacted::from([0_u8; 64]);
            let mut buffer_128 = FullyRedacted::from([0_u8; 128]);
            // Fill them with randomness
            rand(buffer_32.as_mut()).unwrap();
            rand(buffer_64.as_mut()).unwrap();
            rand(buffer_128.as_mut()).unwrap();
            // Round trip them
            let mut cursor = Cursor::new(Vec::new());
            buffer_32.encode(&mut cursor).unwrap();
            cursor.seek(SeekFrom::Start(0)).unwrap();
            let output: FullyRedacted<[u8; 32]> = FullyRedacted::decode(&mut cursor).unwrap();
            assert_eq!(output, buffer_32);
            let mut cursor = Cursor::new(Vec::new());
            buffer_64.encode(&mut cursor).unwrap();
            cursor.seek(SeekFrom::Start(0)).unwrap();
            let output: FullyRedacted<[u8; 64]> = FullyRedacted::decode(&mut cursor).unwrap();
            assert_eq!(output, buffer_64);
            let mut cursor = Cursor::new(Vec::new());
            buffer_128.encode(&mut cursor).unwrap();
            cursor.seek(SeekFrom::Start(0)).unwrap();
            let output: FullyRedacted<[u8; 128]> = FullyRedacted::decode(&mut cursor).unwrap();
            assert_eq!(output, buffer_128);
        }
    }
}
