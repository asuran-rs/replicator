//! Traits used by the file abstraction

/// Provides the encode trait
pub(crate) mod encode;

pub use encode::Encode;
